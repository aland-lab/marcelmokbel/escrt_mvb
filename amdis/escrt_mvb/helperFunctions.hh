#pragma once

#include "amdis/escrt_mvb/SecondOrderGradTestGradTrialAxi.hpp"

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;
using namespace Dune::Indices;

using Grid = Dune::ALUGrid<2,2,Dune::simplex,Dune::ALUGridRefinementType::conforming>;

template <class Container, class GridFunction>
void interpolate(Container& container, GridFunction const& gridFct)
{
    // create a temporary container
    auto tmp = container.coefficients();

    // interpolate the gridFunction
    auto&& gf = makeGridFunction(gridFct, container.basis().gridView());
    Dune::Functions::interpolate(container.basis(), tmp, gf);

    // store the result of the interpolation in the container
    container.coefficients() = std::move(tmp);
}

template <class GridView>
auto vertexCoordinates(GridView const& gridView)
{
    // create a special (discrete) gridFunction with internal storage of coefficients
    Dune::DiscreteGridViewFunction coordinates{gridView, 1};

    // interpolate the coordinate function into this gridFunction container
    Dune::Functions::interpolate(coordinates.basis(), coordinates.coefficients(), [](auto const& x) { return x; });

    return coordinates;
}

/// update DiscreteGridViewFunctions to new grid and set them to zero
template <class DGVF>
void updateZero(DGVF& dgvf) {
    Dune::Functions::interpolate(dgvf.basis(),
                                 dgvf.coefficients(),
                                 [](auto const &x) { return 0.0; });
}

template <class DGVF, class... Args>
void updateZero(DGVF& dgvf, Args... args) {
    Dune::Functions::interpolate(dgvf.basis(),
                                 dgvf.coefficients(),
                                 [](auto const &x) { return 0.0; });
    updateZero(args...);
}

/// update DiscreteGridViewFunctions to new grid and set them to zero
template <class GV, class DGVF>
void updateZero(GV const& gridView, DGVF& dgvf) {
    dgvf.update(gridView);
    Dune::Functions::interpolate(dgvf.basis(),
                                 dgvf.coefficients(),
                                 [](auto const &x) { return 0.0; });
}

template <class GV, class DGVF, class... Args>
void updateZero(GV const& gridView, DGVF& dgvf, Args... args) {
    dgvf.update(gridView);
    Dune::Functions::interpolate(dgvf.basis(),
                                 dgvf.coefficients(),
                                 [](auto const &x) { return 0.0; });
    updateZero(gridView, args...);
}

template <class GV, class DGVF, class DF, class DF2, class SG>
void updateJumps(GV const& gridView,
                 DGVF& deltaP,
                 DGVF& deltaPhi,
                 DF const& phi,
                 DF2 const& p,
                 SG& shellGeometry,
                 std::vector<int> const& partitions,
                 double V0) {
    updateZero(gridView, deltaP, deltaPhi);

    auto dpCorrected = makeDOFVector(valueOf(p).basis());
    std::cout << "compute jump of phi\n";
    jump(gridView, phi, deltaPhi, shellGeometry, partitions, _2, 1);
    auto sigma = Parameters::get<double>("parameters->sigma").value_or(0);
    sigma = 6*sqrt(2)*sigma; //rescale physically correct sigma_ to model sigma
    auto eps = Parameters::get<double>("parameters->eps").value_or(0);
    valueOf(dpCorrected,_1) << p - (sigma * eps * 0.5 * pow(two_norm(gradientOf(phi)), 2) +
                                     sigma / eps * 0.25 * (pow(phi, 2) * pow(1.0 - phi, 2)));
    std::cout << "compute jump of p\n";
    jump(gridView, valueOf(dpCorrected,_1), deltaP, shellGeometry, partitions, _1, 0, V0);

}




/// compute current kappaVec with finite elements
template <class DV, class SG, class Map, class EV>
void computeKappaVecOld(DV& kappaVecOld,
                        SG surfaceGrid,
                        Map const &surfaceToFluidMap,
                        EV const& normal) {
    auto axi = Parameters::get<int>("axisymmetric").value_or(0);
    auto preBasis = composite(power<2>(lagrange<1>(),flatInterleaved()), //coords
                              power<2>(lagrange<1>(),flatInterleaved()),flatLexicographic());//kappaVecOld

    ProblemStat prob("kappaVecOld", *surfaceGrid, preBasis);
    auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0);

    prob.initialize(INIT_ALL);

    // x =
    auto opX = makeOperator(tag::testvec_trialvec{},1.0);
    prob.addMatrixOperator(opX,_0,_0);

    // = oldCoords
    auto opV = makeOperator(tag::testvec{},X(),4);
    prob.addVectorOperator(opV,_0);

    // L =
    auto opL = makeOperator(tag::testvec_trialvec{},(X(1)*axi + 1.0-axi));
    prob.addMatrixOperator(opL,_1,_1);

    // subtract spontaneous curvature if k0 nonzero
    if (k0) {
        auto opSpontaneous1 = makeOperator(tag::testvec{}, k0 * makeGridFunction(normal,prob.gridView()), 4);
        prob.addVectorOperator(opSpontaneous1, _1);
    }

    // <grad x, grad v>
    if (axi) {
        for (int i = 0; i < surfaceGrid->dimensionworld; i++) {
            auto opLB = makeOperator(tag::gradtest_gradtrial_axi{}, X(1));
            prob.addMatrixOperator(opLB, makeTreePath(_1, i), makeTreePath(_0, i));
        }
    } else {
        for (int i = 0; i < surfaceGrid->dimensionworld; i++) {
            auto opLB = makeOperator(tag::gradtest_gradtrial{}, 1.0);
            prob.addMatrixOperator(opLB, makeTreePath(_1, i), makeTreePath(_0, i));
        }
    }

    if (axi) {
        auto opAxi4 = makeOperator(tag::test_trial{},1.0/(X(1)),4);
        prob.addMatrixOperator(opAxi4,makeTreePath(_1,1),makeTreePath(_0,1));


        auto predicate = [](auto const& x){ return x[1] < 1.e-9; }; // define boundary
        int use;
        Parameters::get("axisymmetric dirichlet kappaVecOld",use);
        if (use) prob.addDirichletBC(predicate, makeTreePath(_1,1), makeTreePath(_1,1), 0.0);
    }

    int line = Parameters::get<int>("line mesh").value_or(0);
    double ymin = 100000, ymax = -100000;
    if (line) {
        //find corner coordinates of the grid
        for (auto const &el: elements(prob.gridView())) {
                for (int i = 0; i < el.geometry().corners(); i++) {
                    ymin = std::min(ymin, el.geometry().corner(i)[1]);
                    ymax = std::max(ymax, el.geometry().corner(i)[1]);
                }
            }
        auto predicate = [ymin,ymax](auto const& x){ return x[1] < ymin + 1.e-5 || x[1] > ymax - 1.e-5; }; // define boundary
        prob.addDirichletBC(predicate, makeTreePath(_1,1), makeTreePath(_1,1), 0.0);
    }

    AdaptInfo adaptInfo("adapt");
    prob.assemble(adaptInfo);
    prob.solve(adaptInfo);

    // interpolate solution to DiscreteGridViewFunction
    Dune::DiscreteGridViewFunction result{surfaceGrid->leafGridView(),1},xs{surfaceGrid->leafGridView(),1};
    Dune::Functions::interpolate(result.basis(), result.coefficients(), prob.solution(_1));

    // build DiscreteGridViewFunction on bulk grid
    Dune::DiscreteGridViewFunction kappaVec{kappaVecOld.basis().gridView(),1};
    Dune::Functions::interpolate(kappaVec.basis(),
                                 kappaVec.coefficients(),
                                 [](auto const& x) { return FieldVector<double,2>{0.0,0.0}; });

    // interpolate solution to bulk
    for (auto it : surfaceToFluidMap) {
        kappaVec.coefficients()[it.second] = result.coefficients()[it.first];
    }

    valueOf(kappaVecOld).interpolate(kappaVec,tag::assign{});
    prob.writeFiles(adaptInfo);
}

/// compute the minimum angle on the mesh
template<typename GridView>
double minAngle(GridView const& gridView) {
    //compute minimum angle between two grid points on the grid
    double minangle = 1000;
    for (auto const &el: elements(gridView)) {
        for (int i = 0; i < el.geometry().corners(); i++) {
            auto v1 = el.geometry().corner(i);
            auto v2 = el.geometry().corner((i + 1) % el.geometry().corners());
            auto v3 = el.geometry().corner((i + 2) % el.geometry().corners());
            auto e1 = v2 - v1;
            auto e2 = v3 - v1;
            double angle = acos(dot(e1, e2) / (e1.two_norm() * e2.two_norm()));
            minangle = std::min(minangle, angle);
        }
    }
    std::cout << "Minimum angle on the mesh = " << minangle << "\n";
    return minangle;
}

template<typename GF, class TP>
double cflNumber(GF const& vS, TP treePath, double tau) {
    auto localView = vS.basis().localView();
    double cfl = 0.0;
    for (auto const &el: elements(vS.basis().gridView())) {
        localView.bind(el);

        auto const &node = localView.tree().child(treePath);
        if (node.child(0).size() == 0)
            continue;

        auto face = node.child(0).surfaceFacet();
        auto partition = node.child(0).partition();
        auto intersection = el.template subEntity<1>(face);
        auto geo = intersection.geometry();
        auto length = geo.volume();
        auto t = geo.corner(1) - geo.corner(0);
        auto n = t;
        n[1] = -t[0];
        n[0] = t[1];
        Dune::FieldVector<double,2> vS_t;
        for (std::size_t i = 0; i < node.child(_0).size(); ++i) {
            const auto local_x = node.child(0).localIndex(i);
            const auto local_y = node.child(1).localIndex(i);
            auto vS_x = vS.coefficients().get(localView.index(local_x));
            auto vS_y = vS.coefficients().get(localView.index(local_y));

            //compute the tangential velocity at the current DOF
            vS_t[0] = (1.0 - n[0]*n[0])*vS_x - n[0]*n[1]*vS_y;
            vS_t[1] = (1.0 - n[1]*n[1])*vS_y - n[0]*n[1]*vS_x;
            auto normVS = vS_t.two_norm();

            //compute the cfl number
            cfl = std::max(cfl, normVS / length);
        }
    }
    std::cout << "CFL number = " << cfl << "\n";
    return cfl;
}

/// compute the jump of p and phi across \Gamma
template<class GV, class GF, class DGVF, class ShellGeo, class TP>
void jump(GV gridView, GF&& gridFunction, DGVF& result, ShellGeo& shellGeo, std::vector<int> const& partitions, TP treePath, int cut = 0, double V0 = 0) {
    auto shellGridView = shellGeo.gridView();
    auto &coords = shellGeo.coordinates();
    auto const &indexSet = gridView.grid().levelGridView(0).indexSet();
    auto localViewP = gridFunction.basis().localView(); //local view for pressure/phi basis
    auto localViewV = result.basis().localView(); //local view for continuous across \Gamma basis
    std::vector<unsigned int> idxes;

    std::vector<FieldVector<double,2>> interface;
    //first, compute coordinates, where the interface is on \Gamma
    if (cut) {
        GlobalBasis basisBulk(gridView, lagrange<1>());
        auto phiSurf = makeDOFVector(basisBulk);
        auto outside = Parameters::get<int>("phase field outside").value_or(0);
        auto inside = Parameters::get<int>("phase field inside").value_or(0);
        if (outside) interpolateOneSided(phiSurf, gridFunction, 0, partitions, _0);
        if (inside) interpolateOneSided(phiSurf, gridFunction, 1, partitions, _0);

        Dune::DiscreteGridViewFunction<decltype(gridView), 1> phiBulkDGVF{gridView, 1};
        Dune::Functions::interpolate(phiBulkDGVF.basis(),
                                     phiBulkDGVF.coefficients(),
                                     valueOf(phiSurf));
        Dune::DiscreteGridViewFunction<decltype(shellGridView), 1> phiDGVF{shellGridView, 1};
        shellGeo.interpolateToSurface(phiBulkDGVF.coefficients(), phiDGVF.coefficients());
        auto localViewS = phiDGVF.basis().localView(); //local view for continuous across \Gamma basis

        for (auto const &e: elements(shellGridView)) {
            localViewS.bind(e);

            auto const &node = localViewS.tree();
            auto idx0 = localViewS.index(node.localIndex(0))[0];
            auto idx1 = localViewS.index(node.localIndex(1))[0];
            auto v0 = phiDGVF.coefficients()[idx0];
            auto v1 = phiDGVF.coefficients()[idx1];
            if (v0 > 0.5 && v1 <= 0.5 || v0 <= 0.5 && v1 > 0.5) {
                interface.push_back(e.geometry().center());
            }
        }
    }

    // compute pressure difference in spherical state
    double p0 = 0.0;
    if (!cut) {
        auto sigma0 = Parameters::get<double>("parameters->sigma0").value_or(0.0);
        auto Kb = Parameters::get<double>("bendingStiffness").value_or(0.0);
        auto axi = Parameters::get<int>("axisymmetric").value_or(0);
        auto use = Parameters::get<int>("use p0 for permeability").value_or(0);
        double rCirc = std::sqrt(V0/M_PI);
        auto dow = GV::dimension;
        if (use) {
            if (dow == 2 && !axi)
                p0 = sigma0 / rCirc - 0.5 * Kb / std::pow(rCirc, 3);
            else if (axi || dow == 3)
                p0 = 2.0 * sigma0 / rCirc;
        }
    }

    //now compute jump
    for (auto const &e: elements(gridView)) {
        localViewP.bind(e);
        localViewV.bind(e);

        for (auto const &is: intersections(gridView,e)) {
            if (!is.neighbor())
                continue;

            auto father = e;
            while (father.hasFather())
                father = father.father();

            auto fatherOut = is.outside();
            while (fatherOut.hasFather())
                fatherOut = fatherOut.father();

            auto p = partitions[indexSet.index(father)];
            auto q = partitions[indexSet.index(fatherOut)];

            auto const &nodeP = localViewP.tree().child(treePath);
            auto const &nodeV = localViewV.tree();
            if (p == 1 && q == 0) {
                // result += gridFunction...
                for (int i = 0; i < e.geometry().corners(); ++i) { //assumes that local index == corner index...
                    unsigned int bulk_idx = localViewV.index(nodeV.localIndex(i))[0];
                    unsigned int bulk_idxP = localViewP.index(nodeP.localIndex(i))[0];
                    //check if bulk_idx was visited before, if yes, do nothing
                    if (!(std::find(idxes.begin(), idxes.end(), bulk_idxP) != idxes.end())) {
                        idxes.push_back(bulk_idxP);
                        auto idx = localViewP.index(nodeP.localIndex(i));
                        if (!cut) {
                            result.coefficients()[bulk_idx] += p0 + gridFunction.coefficients().get(idx);
                        } else {
                            auto inside = Parameters::get<int>("phase field inside").value_or(0);
                            auto threshold = Parameters::get<double>("phi jump threshold").value_or(0);
                            if (inside) {
                                auto tru = 1;
                                for (int j = 0; j < interface.size(); ++j) {
                                    tru *= ((e.geometry().corner(i) - interface[j]).two_norm() >
                                            threshold);
                                }
                                result.coefficients()[bulk_idx] += tru * (gridFunction.coefficients().get(idx) < 0.5);
                            }
                        }

                    }
                }
            }
            if (p == 0 && q == 1) {
                // result -= gridFunction...
                for (int i = 0; i < e.geometry().corners(); ++i) { //assumes that local index == corner index...
                    unsigned int bulk_idx = localViewV.index(nodeV.localIndex(i))[0];
                    unsigned int bulk_idxP = localViewP.index(nodeP.localIndex(i))[0];
                    //check if bulk_idx was visited before, if yes, do nothing
                    if (!(std::find(idxes.begin(), idxes.end(), bulk_idxP) != idxes.end())) {
                        idxes.push_back(bulk_idxP);
                        auto idx = localViewP.index(nodeP.localIndex(i));
                        if (!cut) {
                            result.coefficients()[bulk_idx] -= gridFunction.coefficients().get(idx);
                        } else {
                            auto outside = Parameters::get<int>("phase field outside").value_or(0);
                            auto threshold = Parameters::get<double>("phi jump threshold").value_or(0);
                            if (outside) {
                                auto tru = 1;
                                for (int j = 0; j < interface.size(); ++j) {
                                    tru *= ((e.geometry().corner(i) - interface[j]).two_norm() >
                                            threshold);
                                }
                                result.coefficients()[bulk_idx] += tru * (gridFunction.coefficients().get(idx) < 0.5);
                            }
                        }
                    }
                }
            }
        }
        localViewV.unbind();
        localViewP.unbind();
    }
}

/// compute the phase field values on the surfaceGrid (only implemented for one-sided phase field)
template <class DGVFS, class Prob, class ShellGeo>
void phiSurface(DGVFS& phiDGVF, Prob const& nschProb, ShellGeo& shellGeometry, std::vector<int> const& partitions) {
    Dune::DiscreteGridViewFunction<decltype(nschProb.gridView()), 1> phiBulkDGVF{nschProb.gridView(),1};
    DOFVector phiSurf(nschProb.gridView(), lagrange<1>());
    auto outside = Parameters::get<int>("phase field outside").value_or(0);
    auto inside = Parameters::get<int>("phase field inside").value_or(0);

    auto phi = nschProb.solution(_2);
    if (outside) interpolateOneSided(phiSurf,phi,0,partitions,_0);
    if (inside) interpolateOneSided(phiSurf,phi,1,partitions,_0);
    Dune::Functions::interpolate(phiBulkDGVF.basis(), phiBulkDGVF.coefficients(), valueOf(phiSurf));
    shellGeometry.interpolateToSurface(phiBulkDGVF.coefficients(),phiDGVF.coefficients());
}

/// compute the facets vector based on a given partitions vector, the result is the facet number whithin the element
/// for the facet belonging to \Gamma
static double shellSize0 = 0.0;
template <class Grid>
std::vector<int> computeFacets(Grid const& grid, std::vector<int> const& partitions, int doComputeArea = 0, double& size0 = shellSize0) {
    int closedShell = Parameters::get<int>("closed shell").value_or(1); // 1 if closed shell, 0 else
    std::vector<int> facets(partitions.size(),-1);
    double shellSize0 = 0.0;
    if (!closedShell) {
        for(const auto& e : elements(grid.levelGridView(0))) {
            auto const& indexSet = grid.levelGridView(0).indexSet();
            for (const auto& is : intersections(grid.levelGridView(0),e)) {
                if (!is.neighbor())
                    continue;

                auto p = partitions[indexSet.index(e)];
                auto q = partitions[indexSet.index(is.outside())];
                if (p == 0 && q == 1) {
                    facets[indexSet.index(e)] = 1.0;
                    facets[indexSet.index(is.outside())] = 0.0;
                }
            }
        }
    } else {
        facets = partitions;
        if (doComputeArea) {
            // compute initial shell area
            for (const auto &e: elements(grid.levelGridView(0))) {
                auto const &indexSet = grid.levelGridView(0).indexSet();
                for (const auto &is: intersections(grid.levelGridView(0), e)) {
                    if (!is.neighbor())
                        continue;

                    auto p = partitions[indexSet.index(e)];
                    auto q = partitions[indexSet.index(is.outside())];
                    if (p == 0 && q == 1) {
                        size0 += is.geometry().volume();
                    }
                }
            }
            std::cout << "initial shell area = " << size0 << "\n";
        }
    }
    return facets;
}

/// if membrane points on the interface have moved tangentially, correct them
template <class DGVF, class Geo, class DF>
void correctInterfaceMembraneCoords(DGVF& result, Geo& shellGeo, DF const& phi) {
    int count = 0;
    std::list<int> indices = sortShell(shellGeo);
    Dune::DiscreteGridViewFunction coords{shellGeo.gridView(),1};
    auto x = coords.coefficients();
    x = shellGeo.coordinates().coefficients();
    auto ind = indices.begin();
    for (int i = 0; i < indices.size(); i++) {
        Dune::FieldVector<double,2> x0 = x[*std::prev(ind)];
        Dune::FieldVector<double,2> x1 = x[*ind];
        Dune::FieldVector<double,2> x2 = x[*std::next(ind)];
        auto dist1 = two_norm(x2 - x1);
        auto dist2 = two_norm(x1 - x0);

        // if the distances are s.t. one of them is less than 45% of the other, move points
        if (dist1/dist2 < 0.85 || dist2/dist1 < 0.85) {
            auto phaseValue = phi.coefficients()[*ind];
            if (phaseValue >= 0.1 && phaseValue <= 0.9) { // only move interface points
                auto midpoint = 0.5 * (x0 + x2); // the middle of the line between x0 and x2
                auto m = two_norm(
                        midpoint - x0); // the length of the line from x0 to midpoint (or equally x1 to midpoint)
                auto dist = 0.5 * (dist1 + dist2); // the new dist1 and dist2 (both should be equal to dist)
                auto h = std::sqrt(dist * dist - m * m); // the height of the triangle (x0,M,x_new)
                auto vec = midpoint - x0;
                auto vecT = vec;
                vecT[1] = -vec[0]/two_norm(vec);
                vecT[0] = vec[1]/two_norm(vec);
                auto x_new = h * vecT + midpoint;
                auto x_new2 = -h * vecT + midpoint;
                if (two_norm(x_new - x1) < two_norm(x_new2 - x1)) {
                    x[*ind] = x_new;
                } else {
                    x[*ind] = x_new2;
                }
                std::cout << "correct coordinate x = " << x1 << " to x = " << x[*ind] << "\n";
                count++;
            }
        }
        ++ind;
        ++ind; // twice to move every second point
    }
    std::cout << "numer of corrected coordinates: " << count << "\n";
    result.coefficients() = x;
}

template <class Prob>
void adaptTimestepIfNecessary(Prob& nschProb, //the coupled problem
                              AdaptInfo& adaptInfo, //the timestep information
                              double const& tau, //the timestep itself
                              double& vMin, //the volume of the smallest bulk grid element (2D)
                              double const& vAvg, //the average volume of the grid elements around the surface
                              int& countAfterRemeshing, //number of timesteps after the last remeshing
                              int count, //overall number of timesteps
                              int& remeshingJustHappened, //1 if remeshing happened in the last time step
                              int& noRemeshingHappenedBefore) {
    int reduceTimestep = Parameters::get<int>("reduce time step after remeshing").value_or(0);
    int startReduced = Parameters::get<double>("start with reduced time step").value_or(1);
    double reductionInitial= Parameters::get<double>("adapt->time step reduction factor init").value_or(1);
    int numberInitial = Parameters::get<int>("number of time steps for reduction init").value_or(3);
    double reduction = Parameters::get<double>("adapt->time step reduction factor").value_or(1);
    int number = Parameters::get<int>("number of time steps for reduction").value_or(3);

    // reduce the time step size if remeshing has happened in the last time step for number time steps
    if (remeshingJustHappened && reduceTimestep || !remeshingJustHappened && startReduced && !countAfterRemeshing) {
        if (remeshingJustHappened) adaptInfo.setTimestep(tau * reduction);
        else if (startReduced) adaptInfo.setTimestep(tau * reductionInitial);
        nschProb.setMobility(Parameters::get<double>("parameters->M").value_or(1.0));
        remeshingJustHappened = 0;
        if (startReduced) nschProb.setPermeability(0.0);
    }
    countAfterRemeshing++;

    if (countAfterRemeshing == number && !noRemeshingHappenedBefore ) {
        double step = Parameters::get<double>("adapt->timestep").value_or(1);
        nschProb.setMobility(Parameters::get<double>("parameters->M0").value_or(1.0));
        nschProb.setPermeability(Parameters::get<double>("parameters->permeability").value_or(0.0));
        adaptInfo.setTimestep(step);
    }
    if (count == numberInitial && startReduced) {
        int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
        vMin = vAvg * 4 * std::pow(0.5,ref_int);
        adaptInfo.setTimestep(Parameters::get<double>("adapt->timestep").value_or(1));
    }
}

void copyFilesToOutputFolder(std::string& path) {
    // create directory for all the output files
    Parameters::get("output directory", path);
    filesystem::create_directories(path);
    filesystem::create_directories(path + "/backup");
    filesystem::create_directories(path + "/src");

    // copy parameter file to output folder
    std::string parameterFileName, parameterOutputFileName;
    Parameters::get("parameter file name", parameterFileName);
    Parameters::get("parameter output file name", parameterOutputFileName);
    std::filesystem::copy_file(parameterFileName,
                               parameterOutputFileName,
                               std::filesystem::copy_options::overwrite_existing);

    std::filesystem::path src = "./src";
    for (const auto& entry : std::filesystem::recursive_directory_iterator("./src")) {
        if (!(entry.is_directory() || entry.path().string().find("/CMakeFiles/") != std::string::npos)) {
            std::filesystem::path dest = path + "/src";
            std::filesystem::copy(entry.path(),
                                  dest / entry.path().lexically_relative(src),
                                  std::filesystem::copy_options::overwrite_existing);
        }
    }
    std::filesystem::copy("./src",
                          path + "/src",
                          std::filesystem::copy_options::overwrite_existing);
    std::cout << "Copying done!" << std::endl;
    std::filesystem::copy("./amdis",
                          path + "/amdis",
                          std::filesystem::copy_options::recursive |
                          std::filesystem::copy_options::overwrite_existing);
    std::cout << "Copying done!" << std::endl;
}

void copyMeshFileToOutputFolder(double time) {
    if (time < 1.e-10) {
        std::string inputMesh, outputMesh;
        Parameters::get("stokesMesh->macro file name", inputMesh);
        Parameters::get("newMesh->macro file name", outputMesh);
        std::ifstream src(inputMesh, std::ios::binary);
        std::ofstream dst(outputMesh, std::ios::binary);
        dst << src.rdbuf();
    }
}

template <class P, class EV, class DV>
void updateDistances(P const& prob,
                     EV& distances,
                     DV const& oldLambda1,
                     std::vector<int> const& partitions) {
    distances.resizeZero();
    auto localView = oldLambda1.basis().localView();
    auto axi = Parameters::get<int>("axisymmetric").value_or(0);
    auto Ks = Parameters::get<double>("areaShear").value_or(0);


    auto const &indexSet = prob.gridView().indexSet();
    auto const &indexSet0 = prob.gridView().grid().levelGridView(0).indexSet();
    for (auto const &e: elements(prob.gridView())) {
        for (const auto &is: intersections(prob.gridView(), e)) {
            if (!is.neighbor())
                continue;

            auto father = e;
            while (father.hasFather())
                father = father.father();

            auto fatherOut = is.outside();
            while (fatherOut.hasFather())
                fatherOut = fatherOut.father();

            auto p = partitions[indexSet0.index(father)];
            auto q = partitions[indexSet0.index(fatherOut)];
            if (p != q) {
                localView.bind(e);
                auto const &node = localView.tree();
                int subEntityCodim = 1;
                int subEntityIndex = is.indexInInside(); // Local index of codim 1 entity in the inside() entity where intersection is contained in

                auto const &lfe = node.finiteElement();
                auto const &lc = lfe.localCoefficients();
                auto re = Dune::referenceElement(e);

                // get the values of oldLambda1 on the corners
                std::vector<double> vals;
                for (std::size_t i = 0, k = 0; i < lc.size() && k < 2; ++i) {
                    auto localKey = lc.localKey(i);
                    if (re.subEntities(subEntityIndex, subEntityCodim, localKey.codim()).contains(
                            localKey.subEntity())) {
                        vals.push_back(valueOf(oldLambda1).coefficients().get(localView.index(node.localIndex(i))));
                    }
                }
                // compute mean value of the two values on the corners
                auto mean = (vals[1] + vals[0]) * 0.5;

                // multiply surface segment volume with mean
                distances.data()[indexSet.index(e)] = is.geometry().volume() * mean;
                if (axi && !Ks) distances.data()[indexSet.index(e)] *= is.geometry().center()[1];
            }
        }
    }

    for (auto const &e: elements(prob.grid()->hostGrid()->hostGrid().hostGrid()->leafGridView())) {
        auto const &indexSet = prob.grid()->hostGrid()->hostGrid().hostGrid()->leafGridView().indexSet();
        auto const &indexSet0 = prob.grid()->hostGrid()->hostGrid().hostGrid()->levelGridView(0).indexSet();
        for (const auto &is: intersections(prob.grid()->hostGrid()->hostGrid().hostGrid()->leafGridView(), e)) {
            if (!is.neighbor())
                continue;

            auto father = e;
            while (father.hasFather())
                father = father.father();

            auto fatherOut = is.outside();
            while (fatherOut.hasFather())
                fatherOut = fatherOut.father();

            auto p = partitions[indexSet0.index(father)];
            auto q = partitions[indexSet0.index(fatherOut)];
            if (p != q) {
                distances.data()[indexSet.index(e)] /= is.geometry().volume();
                if (axi && !Ks) distances.data()[indexSet.index(e)] /= is.geometry().center()[1];
            }
        }
    }
}


template <class P, class EV>
void updateNormal(P const& prob,
                  EV& normalX,
                  EV& normalY,
                  std::vector<EV>& normalVec,
                  std::vector<int> const& partitions) {
    normalX.resizeZero();
    normalY.resizeZero();

    auto const &indexSet = prob.gridView().indexSet();
    auto const &indexSet0 = prob.gridView().grid().levelGridView(0).indexSet();
    for (auto const &e: elements(prob.gridView())) {
        for (const auto &is: intersections(prob.gridView(), e)) {
            if (!is.neighbor())
                continue;

            auto father = e;
            while (father.hasFather())
                father = father.father();

            auto fatherOut = is.outside();
            while (fatherOut.hasFather())
                fatherOut = fatherOut.father();

            auto p = partitions[indexSet0.index(father)];
            auto q = partitions[indexSet0.index(fatherOut)];
            if (p != q) {
                normalX.data()[indexSet.index(e)] = is.centerUnitOuterNormal()[0];
                normalY.data()[indexSet.index(e)] = is.centerUnitOuterNormal()[1];
            }
        }
    }
    normalVec[0] = normalX;
    normalVec[1] = normalY;
}

template <class P, class DV, class EV>
void updateLambda2(P const& prob,
                   DV& lambda2,
                   DV const& oldLambda2,
                   DV const& y0,
                   EV& distances,
                   std::vector<int> const& partitions) {
    auto axi = Parameters::get<int>("axisymmetric").value_or(0);
    if (axi) {
        valueOf(lambda2) << valueOf(oldLambda2) * invokeAtQP([](double y, double y0) {
                                           return std::abs(y - y0) < 1.e-8 ? 1.0 : y / y0;
                                       },
                                       X(1), valueOf(y0));

        auto const &indexSet = prob.gridView().indexSet();
        auto const &indexSet0 = prob.gridView().grid().levelGridView(0).indexSet();
        auto localView = lambda2.basis().localView();

        for (auto const &e: elements(prob.gridView())) {
            for (const auto &is: intersections(prob.gridView(), e)) {
                if (!is.neighbor())
                    continue;

                auto father = e;
                while (father.hasFather())
                    father = father.father();

                auto fatherOut = is.outside();
                while (fatherOut.hasFather())
                    fatherOut = fatherOut.father();

                auto p = partitions[indexSet0.index(father)];
                auto q = partitions[indexSet0.index(fatherOut)];
                if (p != q) {
                    localView.bind(e);
                    auto const &node = localView.tree();

                    for (std::size_t j = 0; j < e.geometry().corners(); ++j) {
                        auto x = e.geometry().corner(j);
                        if (x[1] < 1.e-10) {
                            auto val = distances.data()[indexSet.index(e)];
                            valueOf(lambda2).coefficients().set(localView.index(node.localIndex(j)), val);
                        }
                    }
                }
            }
        }
        lambda2.finish();
    }
}