#include <filesystem>
#include <amdis/AMDiS.hpp>
#include <amdis/AdaptInstationary.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/StandardProblemIteration.hpp>
#include <amdis/ProblemStatTraits.hpp>
#include <amdis/ElementVector.hpp>
#include <amdis/gridfunctions/ElementGridFunction.hpp>
#include <dune/grid/uggrid.hh>
#include <dune/alugrid/grid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <amdis/io/VTKWriter.hpp>
#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/vtk/datacollectors/discontinuousdatacollector.hh>
#include <dune/curvedgrid/grid.hh>
#include <dune/curvedgrid/gridfunctions/discretegridviewfunction.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/alugrid/common/backuprestore.hh>
#include <amdis/escrt_mvb/subdomainbasis.hh>
#include <amdis/escrt_mvb/surfacebasis.hh>
#include <amdis/surfacebasis/surfacelagrangebasis.hh>
#include <amdis/surfacebasis/SurfaceToBulkInterpolator.hpp>
#include <amdis/surfacebasis/SurfaceDerivative.hpp>
#include "amdis/escrt_mvb/OneSidedInterpolator.hpp"
#include "amdis/escrt_mvb/helperFunctions.hh"
#include "Laplace.hpp"
#include "NavierStokesCahnHilliard.hpp"
#include "ShellForceOperatorTestVec.hpp"
#include "ShellGeometry.hpp"
#include "SurfaceTensionForce.hpp"
#include "BendingForce.hpp"
#include "StretchingForce.hpp"
#include "StretchingForceAxi.hpp"
#include "MeshGenerator.h"
#include "InterpolateGrids.hpp"
#include "amdis/escrt_mvb/hostGridInterpolator.hpp"
#include "amdis/escrt_mvb/writeFiles.hh"
#include "amdis/escrt_mvb/remeshingLoop.hh"


using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;
using namespace Dune::Indices;
/**
 * @brief The main method where the whole simulation happens
 * @param argc
 * @param argv
 * @return 0
 */
int main(int argc, char** argv)
{
    Environment env(argc, argv);

    // time variables
    double time = 0.0;
    double endTime = 1000;
    Parameters::get("adapt->start time", time);
    Parameters::get("adapt->end time", endTime);
    std::vector<double> timesteps;

    std::string path;
    copyFilesToOutputFolder(path);

    // perform axisymmetric simulations?
    auto axi = Parameters::get<int>("axisymmetric").value_or(0);
    auto Ks = Parameters::get<double>("areaShear").value_or(0);

    // the grid type (ALUGrid)
    using Grid = Dune::ALUGrid<2,2,Dune::simplex,Dune::ALUGridRefinementType::conforming>;
    auto const dow = Grid::dimensionworld;

    //read mesh file and store it into newMesh.msh, which is used in the code and updated during remeshing
    copyMeshFileToOutputFolder(time);

    // using a file reader to read the mesh
    MeshCreator<Grid> meshCreator("stokesMesh");

    // store the mesh into a host grid
    auto gridPtr = meshCreator.create();

    // define Partition-Indices of the mesh according to Physical Surface (2D) definition (e.g. from gmsh)
    std::vector<int> partitions = meshCreator.elementIds();
    std::vector<int> boundaryIds = meshCreator.boundaryIds();

    // define facets vector for surface basis (facets = partitions on elements touching the interface, -1 else)
    double shellSize0 = 0.0;
    std::vector<int> facets = computeFacets(*gridPtr,partitions);

    int remeshingJustHappened = 0; //indicate if remeshing has happened in the last time step

    double vMin = 15e15;
    double vAvg;
    int vAvgCount = 0;

    for (auto const& e : elements(gridPtr->leafGridView())) {
        for (auto const& is : intersections(gridPtr->leafGridView(), e)) {
            if (!is.neighbor())
                continue;

            auto p = partitions[gridPtr->leafGridView().indexSet().index(is.inside())];
            auto q = partitions[gridPtr->leafGridView().indexSet().index(is.outside())];
            if (p != q) {
                vAvg += e.geometry().volume();
                vAvgCount++;
                //if (vMin > e.geometry().volume()) vMin = e.geometry().volume();
            }
        }
    }
    vAvg /= vAvgCount;
    int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
    if (time == 0) {
        int startReduced = Parameters::get<int>("start with reduced time step").value_or(0);
        if (startReduced) ref_int = Parameters::get<int>("refinement->interface at start").value_or(10);
        vMin = vAvg * 4 * std::pow(0.5,ref_int);              //minimum element volume of the grid after the initial refinement
        std::cout << "minimum element size (computed): " << vMin << "\n";
    }

    /// loop over remeshing steps
    int count = 1;
    int noRemeshingHappenedBefore = 1;
    if (time > 0) noRemeshingHappenedBefore = 0;
    double volume0, dropletVolume0;
    while (time < endTime) {
        if (time > 0) {
            //update partitions if grid has been swapped
            std::string name;
            if (count==1) {
                name = "newMeshRestore";
            } else {
                name = "newMesh";
            }
            MeshCreator<Grid> meshCreatorNew(name); //unrefined mesh
            auto gridPtrNew = meshCreatorNew.create();
            auto partitionsNew = meshCreatorNew.elementIds();
            auto boundaryIdsNew = meshCreatorNew.boundaryIds();

            partitions = std::move(partitionsNew);
            boundaryIds = std::move(boundaryIdsNew);
            facets.clear();
            facets = computeFacets(*gridPtr,partitions);

            gridPtr.swap(gridPtrNew);

            //restore refinement information
            std::string filename(path + "/backup/gridRefined" + std::to_string(time));
            std::ifstream file(filename.c_str());
            gridPtr->hostGrid()->restore(file);
            gridPtr->update();

            vMin = vAvg * 4 * std::pow(0.5,ref_int);
        }
        // make a DOFVector filled with coordinates as GridFunction for the CurvedGrid
        GlobalBasis basis1(gridPtr->leafGridView(), power<2>(lagrange<1>(),flatInterleaved()));
        auto coordinates = std::make_shared<DOFVector<decltype(basis1)>>(basis1);
        valueOf(*coordinates) << X();

        // the moving mesh...changing coordsDF updates the mesh!
        Dune::CurvedGrid grid{*gridPtr, valueOf(*coordinates)};

        //define pre basis factory for Navier-Stokes problem
        auto deg = Parameters::get<int>("polynomial degree ch").value_or(1);

        auto pbf = composite(power<2>(lagrange<2>(),flatInterleaved()),//velocity
                             surface(lagrange<1>(), facets),//pressure
                             surface(lagrange(deg), facets),//phi
                             surface(lagrange(deg), facets),//mu
                             power<2>(Dune::Surfacebasis::surfaceLagrange<1>(partitions),flatInterleaved()),//newCoords
                             power<2>(Dune::Surfacebasis::surfaceLagrange<1>(partitions),flatInterleaved()),//kappaVec
                             Dune::Surfacebasis::surfaceLagrange<1>(partitions),//lambda1 (principal stretches)
                             Dune::Surfacebasis::surfaceLagrange<1>(partitions),//lambda2
                             power<2>(Dune::Surfacebasis::surfaceLagrange<1>(partitions),flatInterleaved()),//force
                             power<2>(Dune::Surfacebasis::surfaceLagrange<1>(partitions),flatInterleaved()),//vS
                             Dune::Surfacebasis::surfaceLagrange<1>(partitions),//phiS
                                     flatLexicographic());

        //define Problem for Navier-Stokes Cahn-Hilliard with basis on subdomains
        NavierStokesCahnHilliard nschProb("stokes", grid, pbf, boundaryIds, partitions);

        //define Problem for Laplace with basis on subdomains, using the Partitions-Vector
        using Curved = decltype(grid);
        Laplace<Curved> laplaceProb("laplace", nschProb.grid(), partitions, boundaryIds);

        // initialize the data for the problem(s)
        nschProb.initialize(INIT_ALL);
        laplaceProb.initialize(INIT_ALL);

        AdaptInfo adaptInfo("adapt");
        adaptInfo.setTime(time);

        nschProb.initBaseProblem(adaptInfo);
        laplaceProb.initBaseProblem(adaptInfo);
        nschProb.initTimeInterface();
        laplaceProb.initTimeInterface();

        // the time step
        auto tau = std::ref(adaptInfo.timestep());

        // the grid view and level 0 gridView of the nschProb
        auto const &gridView = nschProb.gridView();
        auto const &level0GridView = nschProb.problem().grid()->levelGridView(0);
        using GV = decltype(level0GridView);

        // the solution variables
        auto fS = nschProb.solution(_8);
        auto mu = nschProb.solution(_3);
        auto phi = nschProb.solution(_2);
        auto p = nschProb.solution(_1);
        auto v = nschProb.solution(_0);
        auto u = laplaceProb.solution();

        // kappaVec of the old time step but on the updated grid!
        DOFVector kappaVecOld(nschProb.gridView(), power<2>(Dune::Surfacebasis::surfaceLagrange<1>(partitions),flatInterleaved()), tag::no_datatransfer{});
        DOFVector kappaVecOldBulk(nschProb.gridView(), power<2>(lagrange<1>(),flatInterleaved()), tag::no_datatransfer{});

        //set old velocity, phase field, etc
        DOFVector oldVelocity(nschProb.gridView(), power<2>(lagrange<2>(),flatInterleaved()));
        DOFVector oldPhi(nschProb.gridView(), surface(lagrange(deg),facets),tag::no_datatransfer{});
        DOFVector oldMu(nschProb.gridView(), surface(lagrange(deg),facets),tag::no_datatransfer{});
        DOFVector oldLambda1(nschProb.gridView(), lagrange<1>());
        DOFVector oldLambda2(nschProb.gridView(), lagrange<1>());
        DOFVector distancesDOF(nschProb.gridView(), lagrange<1>());
        DOFVector distancesDOFS(nschProb.gridView(), power<1>(Dune::Surfacebasis::surfaceLagrange<1>(partitions), flatInterleaved()));

        DOFVector lambda2(nschProb.gridView(), lagrange<1>());
        DOFVector y0(nschProb.gridView(), lagrange<1>());
        valueOf(y0) << X(1); // set y0 to y at start;

        ElementVector distances(*nschProb.grid(), 0.0);
        ElementVector normalX(*nschProb.grid(),0.0);
        ElementVector normalY(*nschProb.grid(),0.0);
        std::vector<decltype(normalX)> normalVec;
        normalVec.push_back(normalX);
        normalVec.push_back(normalY);
        updateDistances(nschProb, distances, oldLambda1, partitions);
        updateNormal(nschProb, normalX, normalY, normalVec, partitions);
        valueOf(distancesDOFS).interpolate(valueOf(distances) - 1.0, tag::average{});
        valueOf(distancesDOF).interpolate(valueOf(distancesDOFS,_0),tag::assignToBulk{});
        valueOf(distancesDOF) += 1.0;

        if (axi && Ks) updateLambda2(nschProb, lambda2, oldLambda2, y0, distances,  partitions);

        if (time > 0) {
            oldVelocity.restore(path + "/backup/uhOld" + std::to_string(time));
            oldPhi.restore(path + "/backup/phiOld" + std::to_string(time));
            oldMu.restore(path + "/backup/muOld" + std::to_string(time));
            oldLambda1.restore(path + "/backup/lambda1Old" + std::to_string(time));
            oldLambda2.restore(path + "/backup/lambda2Old" + std::to_string(time));
            nschProb.solution(_0) << valueOf(oldVelocity);
            nschProb.solution(_2) << valueOf(oldPhi);
            nschProb.solution(_3) << valueOf(oldMu);

            updateDistances(nschProb, distances, oldLambda1, partitions);
            distancesDOF.resizeZero();
            valueOf(distancesDOFS).interpolate(valueOf(distances) - 1.0, tag::average{});
            valueOf(distancesDOF).interpolate(valueOf(distancesDOFS,_0),tag::assignToBulk{});
            valueOf(distancesDOF) += 1.0;

            if (axi && Ks) updateLambda2(nschProb, lambda2, oldLambda2, y0, distances,  partitions);

            remeshingJustHappened = 1;
        } else {
            valueOf(oldLambda1) << 1.0;
            valueOf(oldLambda2) << 1.0;
        }

        // average shell velocity and laplace solution to be subtracted from convection term
        auto center = Parameters::get<int>("parameters->keep membrane centered").value_or(0);
        double const &vx = nschProb.vx();
        double const &vy = nschProb.vy();
        auto v_cell = [&vx, &vy, &center](auto const &x) {
            return FieldVector<double, 2>{-center * vx, -center * vy};
        }; //0 if no correction, -vx if shell is centered

        // add ALE operators to the NavierStokesCahnHilliard Problem
        nschProb.fillCouplingOperatorALE(u, v_cell);

        // print grid information
        msg("number of grid points: {}, number of faces: {}, number of elements: {}",
            grid.size(2), grid.size(1), grid.size(0));
        msg("nschProb number of DOFs: {}", nschProb.globalBasis()->dimension());

        /// extract surface mesh from bulk mesh
        // setting up surface grid
        using SurfaceGrid = Dune::FoamGrid<1, 2>;
        std::map<int, int> surfaceToFluidMap;

        // DiscreteGridViewFunction of Coordinates on nschProb grid (incl. old coordinates of last time step)
        auto coordsDiscrete = vertexCoordinates(gridView);

        // helper vector for determination of outward direction of normals
        std::vector<FieldVector<double, dow>> elementNormalsDirection;

        // create surface mesh, fill surfaceToFluidMap, fill elementNormalsDirection
        std::unique_ptr surfaceGrid = createSurfaceMesh<SurfaceGrid>(partitions,
                                                                     coordsDiscrete,
                                                                    surfaceToFluidMap,
                                                                    elementNormalsDirection);

        msg("surfaceGrid: vertices={}, elements={}", surfaceGrid->size(1), surfaceGrid->size(0));

        // create shell geometry object (for normals and curvature)
        ShellGeometry<SurfaceGrid> shellGeometry{*surfaceGrid,
                                                 surfaceToFluidMap,
                                                 elementNormalsDirection};

        // build gridFunctions for pressure jump and phase field jump
        Dune::DiscreteGridViewFunction<decltype(nschProb.gridView()), 1> deltaP{nschProb.gridView(),1};
        Dune::DiscreteGridViewFunction<decltype(nschProb.gridView()), 1> deltaPhi{nschProb.gridView(),1};
        Dune::DiscreteGridViewFunction n{nschProb.gridView(),1};
        Dune::DiscreteGridViewFunction<decltype(nschProb.gridView()), 1> gaussian{nschProb.gridView(), 1};

        //jump initially zero, computed after each time step
        updateZero(deltaP, deltaPhi);

        // compute normals on \Gamma and interpolate to bulk function n
        shellGeometry.interpolateToBulk(n.coefficients(),shellGeometry.surfaceNormal().coefficients());
        shellGeometry.interpolateToBulk(gaussian.coefficients(), shellGeometry.curvature(true).coefficients());

        // add boundary condition for laplace problem on the shell (incl. permeability)
        auto displacement = makeGridFunction(tau * u, gridView);

        auto K = Parameters::get<double>("parameters->permeability").value_or(0);
        auto normalMovement = Parameters::get<int>("move grid points with normal velocity").value_or(0);
        if (normalMovement) {
            auto v_corrected = makeGridFunction(dot(v,n)*n + K*deltaP*abs(deltaPhi)*n + v_cell, gridView);
            laplaceProb.problem().addConstraint(
                    MeshMovementBC{*laplaceProb.problem().globalBasis(), partitions, v_corrected});
        } else {
            auto v_corrected = makeGridFunction(v + K*deltaP*abs(deltaPhi)*n + v_cell, gridView);
            laplaceProb.problem().addConstraint(
                    MeshMovementBC{*laplaceProb.problem().globalBasis(), partitions, v_corrected});
        }
        // add surface operators
        nschProb.fillSurfaceOperators(deltaP, deltaPhi, kappaVecOld, normalVec);

        // indicators, if explicit forces are used
        auto explST = Parameters::get<int>("explicit surface tension force").value_or(0);
        auto explS = Parameters::get<int>("explicit stretching force").value_or(0);
        auto explB = Parameters::get<int>("explicit bending force").value_or(0);

        // add surface tension force
        SurfaceTensionForce surfaceTensionForce(shellGeometry, gridView);
        nschProb.fillSurfaceTensionForceOperators(surfaceTensionForce, normalVec);

        // add stretching force
        //StretchingForce stretchingForce(shellGeometry, gridView, partitions);
        StretchingForceAxi stretchingForceAxi(shellGeometry, gridView, partitions);
        nschProb.fillStretchingForceOperators(stretchingForceAxi,
                                              distances,
                                              lambda2,
                                              kappaVecOld,
                                              normalVec);

        // add bending stiffness force
        BendingForce bendingForce(shellGeometry, gridView, shellGeometry.gridView(), partitions);
        nschProb.fillBendingForceOperators(bendingForce, kappaVecOld, gaussian, normalVec);

        nschProb.initData(adaptInfo,
                          makeGridFunction(valueOf(distances) - 1.0,nschProb.gridView()),
                          vMin);
        if (time == 0) shellSize0 = nschProb.integrateShell(1.0,nschProb.gridView(),partitions);

        nschProb.setShellArea0(shellSize0);

        // perform initial refinement
        nschProb.solveInitialProblem(adaptInfo);
        if (time == 0) {
            dropletVolume0 = nschProb.getDropletVolume0();
            volume0 = nschProb.getVolume0();
            //vMin = nschProb.getMinimumElementVolume();
        } else if (remeshingJustHappened) {
            if (volume0 == 0) volume0 = nschProb.getVolume0();
            if (dropletVolume0 == 0) dropletVolume0 = nschProb.getDropletVolume0();
            nschProb.setDropletVolume0(dropletVolume0);
            nschProb.setVolume0(volume0);
            std::cout << "minimum element volume = " << vMin << "\n";
        }

        // update shellGeometry to refined grid
        shellGeometry.updateGrid(coordsDiscrete,nschProb.gridView(),partitions);

        msg("surfaceGrid: vertices={}, elements={}", shellGeometry.surfaceGrid()->size(1),
            shellGeometry.surfaceGrid()->size(0));

        // update lambdas, n, deltas to new grid
        updateZero(nschProb.gridView(), n, gaussian, deltaP, deltaPhi);

        std::cout << "compute jump of p\n";
        double V0 = nschProb.volume();
        jump(nschProb.gridView(),p,deltaP,shellGeometry,partitions,_1,0,V0);
        std::cout << "compute jump of phi\n";
        jump(nschProb.gridView(),phi,deltaPhi,shellGeometry,partitions,_2,1);

        /// make data for file writers
        // create a vtkSequenceWriter for paraview animation (.pvd) files
        Dune::VTKWriter bulkWriter(gridView, Dune::VTK::nonconforming);
        Dune::VTKWriter surfWriter(shellGeometry.gridView(), Dune::VTK::conforming);

        // store the pressure, velocity and laplace solution into the writer
        addWriterData(bulkWriter,p,"p",1,
                                 v,"v",2,
                                 phi,"phi",1,
                                 mu,"mu",1,
                                 valueOf(distancesDOF),"lambda1Old",1,
                                 valueOf(lambda2),"lambda2Old",1,
                                 deltaP,"deltaP",1,
                                 deltaPhi,"deltaPhi",1,
                                 nschProb.nuPhase(),"nuPhase",1);

        if (explB)
            addWriterData(bulkWriter,bendingForce.getForce(gridView),"bendingForce",2);

        if (explST)
            addWriterData(bulkWriter,surfaceTensionForce.getForce(gridView),"surfaceTensionForce",2);

        if (explS)
            addWriterData(bulkWriter,stretchingForceAxi.getForce(gridView),"stretchingForce",2);

        // store surface normals and surface curvature into surface writer
        auto const &spb = nschProb.solution(_5).basis().preBasis().subPreBasis(_5).subPreBasis();
        auto kOld = surfaceGridFct2D(kappaVecOld, spb);

        addWriterData(surfWriter,shellGeometry.surfaceNormal(),"normals",2,
                      shellGeometry.curvature(),"kappa",1,
                      shellGeometry.laplaceKappa(),"laplaceKappa",1,
                      kOld,"KappaTimesNOld",2);
        addWriterDataSurface(surfWriter,nschProb);


        if (adaptInfo.time() == 0) {
            // write (full) solution to (.pvd) file
            timesteps.push_back(adaptInfo.time());
            writePVDFile<decltype(bulkWriter)>(timesteps, "bulk",  path, bulkWriter, 2);
            writePVDFile<decltype(surfWriter)>(timesteps, "surface",  path, surfWriter, 1);
        } else {
            //write the file of the actual time step
            bulkWriter.write( path + "/bulkAfterLastRemeshing");
            surfWriter.write( path + "/surfaceAfterLastRemeshing");
        }

        int countAfterRemeshing = 0;

        /// the actual adaptation loop
        while (adaptInfo.time() < adaptInfo.endTime()) {
            Dune::Timer timestepTime;

            //depending on parameter in initfile, change timestep size in the first few time steps and/or after remeshing
            adaptTimestepIfNecessary(nschProb,
                                     adaptInfo,
                                     tau,
                                     vMin,
                                     vAvg,
                                     countAfterRemeshing,
                                     count,
                                     remeshingJustHappened,
                                     noRemeshingHappenedBefore);

            adaptInfo.incTimestepNumber();
            adaptInfo.setTime(adaptInfo.time() + tau);
            time = adaptInfo.time();

            // time information
            std::cout << "time = " << adaptInfo.time() << ", end time = " << adaptInfo.endTime() << ", timestep = "
                      << tau << ", timestep number = " << count << "\n";

            // print shell area, velocity and volume
            std::cout << "shell area = " << nschProb.getShellArea() << ", shell velocity = " << vx
                      << ", shell volume = "
                      << nschProb.volume() << "\n";


            /// ns iteration
            nschProb.initTimestep(adaptInfo);
            nschProb.beginIteration(adaptInfo);
            ///nschProb.oneIteration(adaptInfo);
            {
                nschProb.problem().beginIteration(adaptInfo);

                nschProb.buildAndAdapt(adaptInfo);

                /// compute kappaVecOld with finite elements
                auto sGrid = nschProb.problem().solution(_8).basis().preBasis().subPreBasis(_8).subPreBasis().surfaceGrid();
                auto const &map = nschProb.globalBasis()->preBasis().subPreBasis(
                        _8).subPreBasis().surfaceToFluidMapIdx();

                computeKappaVecOld(kappaVecOld, sGrid, map, shellGeometry.surfaceNormal());

                valueOf(kappaVecOldBulk).interpolate(valueOf(kappaVecOld),tag::assignToBulk{});

                /// update shellGeometry to refined grid
                shellGeometry.updateGrid(coordsDiscrete,nschProb.gridView(),partitions);

                ///update deltaP and deltaPhi and normal on bulk grid
                updateZero(nschProb.gridView(), n, gaussian);
                updateJumps(nschProb.gridView(), deltaP, deltaPhi, phi, p, shellGeometry, partitions,V0);

                // compute normals on the surface and interpolate to the bulk
                shellGeometry.interpolateToBulk(n.coefficients(), shellGeometry.surfaceNormal().coefficients());
                shellGeometry.interpolateToBulk(gaussian.coefficients(), shellGeometry.curvature(true).coefficients());

                updateDistances(nschProb, distances, oldLambda1, partitions);
                updateNormal(nschProb, normalX, normalY, normalVec, partitions);
                if (axi && Ks) updateLambda2(nschProb, lambda2, oldLambda2, y0, distances, partitions);

                nschProb.assembleAndSolve(adaptInfo);
                nschProb.problem().endIteration(adaptInfo);
            }

            msg("number of grid points: {}, number of faces: {}, number of elements: {}",
                grid.size(2), grid.size(1), grid.size(0));

            nschProb.endIteration(adaptInfo);
            nschProb.closeTimestep(adaptInfo);

            ///update deltaP and deltaPhi and normal on bulk grid
            updateJumps(nschProb.gridView(), deltaP, deltaPhi, phi, p, shellGeometry, partitions,V0);


            /// laplace iteration
            laplaceProb.initTimestep(adaptInfo);
            laplaceProb.beginIteration(adaptInfo);
            laplaceProb.oneIteration(adaptInfo);
            laplaceProb.endIteration(adaptInfo);
            laplaceProb.closeTimestep(adaptInfo);

            Dune::Timer t;
            // move the grid with solution of Laplace as displacement
            // actual grid update: the values of coordsDiscrete are set so the coodinates DOFVector
            DOFVector coords(nschProb.gridView(),power<2>(lagrange(1),flatInterleaved()));
            valueOf(coords) << X() + displacement;
            interpolateHostGrid(valueOf(*coordinates),valueOf(coords));
            AMDiS::info(2, "grid movement needed {} seconds", t.elapsed());

            // coordsDiscrete -> coordinate DircreteGridViewFunction on CurvedGrid grid
            coordsDiscrete.update(nschProb.gridView());
            coordsDiscrete = vertexCoordinates(nschProb.gridView());
            interpolate(coordsDiscrete, valueOf(coords));
            AMDiS::info(2, "grid movement preparation needed {} seconds", t.elapsed());

            // update forces
            if (explST) surfaceTensionForce.updateForce(nschProb.gridView());
            if (explB) bendingForce.updateForce(nschProb.gridView());
            if (explS) stretchingForceAxi.updateForce(nschProb.gridView());

            // update (surface) basis after grid movement
            nschProb.globalBasis()->update(nschProb.gridView());

            // update shellGeometry to new grid
            shellGeometry.updateGrid(coordsDiscrete,nschProb.gridView(),partitions);

            msg("surfaceGrid: vertices={}, elements={}", shellGeometry.surfaceGrid()->size(1),
                shellGeometry.surfaceGrid()->size(0));

            updateDistances(nschProb, distances, oldLambda1, partitions);
            updateNormal(nschProb, normalX, normalY, normalVec, partitions);
            distancesDOF.resizeZero();
            valueOf(distancesDOFS).interpolate(valueOf(distances) - 1.0, tag::average{});
            valueOf(distancesDOF).interpolate(valueOf(distancesDOFS,_0),tag::assignToBulk{});
            valueOf(distancesDOF) += 1.0;

            if (axi && Ks) updateLambda2(nschProb, lambda2, oldLambda2, y0, distances, partitions);

            AMDiS::info(2, "data update needed {} seconds", t.elapsed());

            /// write the files
            int every = 1;
            Parameters::get("every", every);
            if (count % every == 0) {
                Dune::Timer t;
                // create a new surfaceWriter, since grid has changed
                Dune::VTKWriter surfWriter(shellGeometry.gridView(), Dune::VTK::conforming);

                auto const &spb = nschProb.solution(_5).basis().preBasis().subPreBasis(_5).subPreBasis();
                kOld = surfaceGridFct2D(kappaVecOld, spb);

                addWriterData(surfWriter, shellGeometry.surfaceNormal(), "normals", 2,
                              shellGeometry.curvature(), "kappa", 1,
                              shellGeometry.laplaceKappa(), "laplaceKappa", 1,
                              kOld, "KappaTimesNOld", 2);

                addWriterDataSurface(surfWriter, nschProb);

                timesteps.push_back(time);
                writePVDFile<decltype(bulkWriter)>(timesteps, "bulk", path, bulkWriter, 2);
                writePVDFile<decltype(surfWriter)>(timesteps, "surface", path, surfWriter, 1);

                AMDiS::info(2, "write File step needed {} seconds", t.elapsed());
            }
            std::cout << "\n";
            AMDiS::info(1, "time step {} needed {} seconds", count, timestepTime.elapsed());
            AMDiS::info(1, "====================================================================");
            std::cout << "\n";
            count++;

            /// if necessary and desired, correct surface interface grid points if they have moved due to spurious currents...
            auto doCorrect = Parameters::get<int>("correct point distances").value_or(0);
            if (doCorrect) {
                Dune::DiscreteGridViewFunction<decltype(shellGeometry.gridView()), 1> phiDGVF{shellGeometry.gridView(),
                                                                                              1};
                phiSurface(phiDGVF, nschProb, shellGeometry, partitions);
                correctInterfaceMembraneCoords(shellGeometry.coordinates(), shellGeometry, phiDGVF);
                auto displShell = coordsDiscrete;
                shellGeometry.interpolateToBulk(coordsDiscrete.coefficients(),
                                                shellGeometry.coordinates().coefficients());
                valueOf(coords) << coordsDiscrete;
                interpolateHostGrid(valueOf(*coordinates), valueOf(coords));
                u += (coordsDiscrete - displShell) / tau;
            }
            /// if necessary, perform remeshing and leave the current loop to restart the simulation with
            /// the backup files from the remeshing function
            if (remeshing(nschProb, gridPtr, shellGeometry,partitions,
                          facets,boundaryIds,timesteps,adaptInfo,path,vMin,
                          distances, lambda2)) {
                noRemeshingHappenedBefore = 0;
                break;
            }
        }
    }
}

// include ccache with:  -DCMAKE_CXX_COMPILER_LAUNCHER=ccache