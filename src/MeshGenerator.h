//
// Created by Marcel Mokbel on 22.06.2021.
//

#ifndef escrt_mvb_MESHGENERATOR_H
#define escrt_mvb_MESHGENERATOR_H
#include <set>
#include <cmath>
#include <gmsh.h>
#include <iostream>
#include <iomanip>

template <class GEO>
std::list<int> sortShell(GEO &shellGeo) {
    std::list<int> indicesSorted;
    auto shellCoords = shellGeo.coordinates().coefficients();
    auto nIdx = shellGeo.neighborIndices();
    int startIdx = Parameters::get<int>("sortShell start index").value_or(0);
    int idx = startIdx;
    int idxRight = 0;
    int axi = Parameters::get<int>("axisymmetric").value_or(0);
    int line = Parameters::get<int>("line mesh").value_or(0);
    double ymin = 100000;
    // axisymmetric - find starting point: right point touching symmetry axis
    if (axi) {
        std::vector<FieldVector<double,2>> pointsOnSyxmmetryAxis;
        std::vector<int> idxOnSymmetryAxis;
        for (int i = 0; i < shellCoords.size(); i++) {
            if (nIdx[i].size() < 2) { //if there is only one neighbor ->shell points touching symmetry axis
                pointsOnSyxmmetryAxis.push_back(shellCoords[i]);
                idxOnSymmetryAxis.push_back(i);
            }
        }
        if (pointsOnSyxmmetryAxis[0][0] < pointsOnSyxmmetryAxis[1][0]) {
            idxRight = idxOnSymmetryAxis[1];
        } else {
            idxRight = idxOnSymmetryAxis[0];
        }
    }
    if (line) {
        for (int i = 0; i < shellCoords.size(); i++) {
            if (nIdx[i].size() < 2) { //if there is only one neighbor ->shell points touching boundary of mesh
                if (shellCoords[i][1] < ymin) { //find lower point
                    idxRight = i;
                    ymin = shellCoords[i][1];
                }
            }
        }
    }

    if (axi || line) idx = idxRight;
    indicesSorted.push_back(idx);
    for (int i = 0; i < shellCoords.size() - 1; i++) {
        idx = nIdx[idx][0]; //right neighbor index of i
        indicesSorted.push_back(idx);
    }
    return indicesSorted;
}

// remove shell points away from the phase field interface that have been made in previous remeshing steps
// and are too close to each other
// remove points on the interface that are closer than 1/3 of the distance given by the element size, assuming that
// an element with given size is a perfect triangle with equal sides
template <class DF, class DF2>
void removeClosestPoints(DF const& coords, std::list<int> &indices, DF2 const& phi, double vMin) {
    auto doRemove = Parameters::get<int>("remove interface points").value_or(0);
    auto axi = Parameters::get<int>("axisymmetric").value_or(0);
    auto line = Parameters::get<int>("line mesh").value_or(0);
    if (doRemove) {
        auto ind = indices.begin();
        auto x = coords.coefficients();
        double distMin = 0, distMaxSurface = 0;
        int counter = 0;
        ind = indices.begin();
        std::vector<FieldVector<double,DF::Range::dimension>> iFaceLocations;
        while (ind != indices.end()) { //find locations where interface is (phi=0.5)
            auto phaseValue = phi.coefficients()[*ind];
            auto phaseValue2 = phi.coefficients()[*std::next(ind)];
            if (*ind == indices.back() ) phaseValue2 = phaseValue;
            if ((phaseValue < 0.5 && phaseValue2 >= 0.5) || (phaseValue2 < 0.5 && phaseValue >= 0.5))
            {
                iFaceLocations.push_back(0.5*(x[*std::next(ind)] + x[*ind]));
            }
            if (*ind != indices.back()) {
                auto dist = two_norm(x[*std::next(ind)] - x[*ind]);
                if (phaseValue > 0.025 && phaseValue < 0.975) { //compute mean length on phase field interface
                    distMin += dist;
                    counter++;
                }
                if (dist > distMaxSurface) distMaxSurface = dist;
            }
            ind++;
        }
        distMin /= 3.0*counter;

        ind = indices.begin();
        while (ind != indices.end()) {
            auto dist1 = two_norm(x[*std::next(ind)] - x[*ind]);
            if (*ind == indices.back() && (axi || line)) dist1 = two_norm(x[*ind] - x[*std::prev(ind)]);
            if (*ind == indices.back() && !axi) dist1 =  two_norm(x[*ind] - x[indices.front()]);
            auto dist2 = two_norm(x[*ind] - x[*std::prev(ind)]);
            if (ind == indices.begin() && (axi || line)) dist2 = dist1;
            if (ind == indices.begin() && !axi) dist2 =  two_norm(x[*ind] - x[indices.back()]);
            auto maxDist = Parameters::get<double>("distance for removal of interface points").value_or(0);
            auto phaseValue = phi.coefficients()[*ind];
            maxDist *= 3.0 * distMin;

            double distIface = 100000000000;
            for (int k = 0; k < iFaceLocations.size(); k++)
                if (two_norm(x[*ind]-iFaceLocations[k]) < distIface) distIface = two_norm(x[*ind]-iFaceLocations[k]);
            if (distIface > 40*distMin) {
                if (std::abs(x[*ind][1]) > 1e-7 && ((dist1 + dist2) * 0.5 < maxDist) &&
                    (phaseValue < 0.05 || phaseValue > 0.95)
                    || ((dist1 < distMin || dist2 < distMin) &&
                        (phaseValue >= 0.05 && phaseValue <= 0.95))) {
                    std::cout << dist1 << ", " << dist2 << ", " << maxDist  << ", " << distIface << ", " << distMin << ", "
                              << vMin << ", " << x[*std::prev(ind)] << ", " << x[*ind] << ", " << x[*std::next(ind)]
                              << "\n";
                    ind = indices.erase(ind);
                } else {
                    ++ind;
                }
            } else {
                if (std::abs(x[*ind][1]) > 1e-7 && ((dist1 + dist2) * 0.5 < 3.0*distMin) &&
                    (phaseValue < 0.05 || phaseValue > 0.95)
                    || ((dist1 < distMin || dist2 < distMin) &&
                        (phaseValue >= 0.05 && phaseValue <= 0.95))) {
                    std::cout << dist1 << ", " << dist2 << ", " << maxDist << ", " << distIface << ", " << distMin << ", "
                              << vMin << ", " << x[*std::prev(ind)] << ", " << x[*ind] << ", " << x[*std::next(ind)]
                              << "\n";
                    ind = indices.erase(ind);
                } else {
                    ++ind;
                }
            }
        }

        while (ind != indices.end()) {// remove if small distanced points are still left...
            auto dist2 = two_norm(x[*std::next(ind)] - x[*ind]);
            if (*ind == indices.back() && (axi || line)) dist2 = two_norm(x[*ind] - x[*std::prev(ind)]);
            if (*ind == indices.back() && !axi) dist2 =  two_norm(x[*ind] - x[indices.front()]);

            auto maxDist = Parameters::get<double>("distance for removal of interface points").value_or(0);
            auto distMin = sqrt(4.0 / sqrt(3) * vMin) / 3.0; //side length of a triangle with equal sides / 3
            maxDist *= 3.0 * distMin;
            auto phaseValue = phi.coefficients()[*ind];
            if (std::abs(x[*ind][1]) > 1e-7 && (dist2 < maxDist) && (phaseValue < 0.05 || phaseValue > 0.95)) {
                std::cout << dist2 << ", " << maxDist << ", " << distMin << ", "
                          << vMin << ", " << x[*std::prev(ind)] << ", " << x[*ind] << ", " << x[*std::next(ind)] << "\n";
                ind = indices.erase(ind);
            } else {
                ++ind;
            }
        }
    }
}


template <class GEO, class DF, class GV>
void generateMesh(std::string meshName, std::string meshDir,
                  GEO &shellGeo,
                  DF const& phi,
                  double time,
                  GV const& gridView,
                  double vMin) {
    std::list<int> indices = sortShell(shellGeo);
    auto shellCoords = shellGeo.coordinates().coefficients();
    std::cout << "indices size before removal " << indices.size() << "\n";
    removeClosestPoints(shellGeo.coordinates(), indices, phi, vMin);
    std::cout << "indices size after removal " << indices.size() << "\n";
    auto h = Parameters::get<double>("mesh resolution at shell").value_or(1.0);
    auto hout = Parameters::get<double>("mesh resolution at boundary").value_or(1.0);
    auto hbox = Parameters::get<double>("mesh resolution at box").value_or(1.0);
    auto axi = Parameters::get<int>("axisymmetric").value_or(1);
    auto line = Parameters::get<int>("line mesh").value_or(0);

    //find corner coordinates of the grid
    double xmin=100000,ymin=100000,xmax=-100000,ymax=-100000, distMax=0;
    for (auto const& el : elements(gridView)) {
        for (auto const& is : intersections(gridView,el)) {
            if (is.neighbor()) //only consider boundary intersections
                continue;
            for (int i = 0; i<is.geometry().corners(); i++) {
                xmin = std::min(xmin,is.geometry().corner(i)[0]);
                ymin = std::min(ymin,is.geometry().corner(i)[1]);
                xmax = std::max(xmax,is.geometry().corner(i)[0]);
                ymax = std::max(ymax,is.geometry().corner(i)[1]);
            }
            auto dist = two_norm(is.geometry().corner(1) - is.geometry().corner(0));
            if (dist > distMax) distMax = dist;
        }
    }
    //find distances between two adjacent shell grid points
    auto ind = indices.begin();
    double distMin = 0, distMaxSurface = 0;
    int counter = 0;
    while (ind != indices.end()) {
        auto phaseValue = phi.coefficients()[*ind];
        if (*ind != indices.back()) {
            auto dist = two_norm(shellCoords[*std::next(ind)] - shellCoords[*ind]);
            if (phaseValue > 0.025 && phaseValue < 0.975) { //compute mean length on phase field interface
                distMin += dist;
                counter++;
            }
            if (dist > distMaxSurface) distMaxSurface = dist;
        }
        ++ind;
    }
    distMin /= counter;

    gmsh::initialize();

    gmsh::option::setNumber("General.Terminal", 1); //set to 0 for no output
    gmsh::model::add(meshName);

    gmsh::model::geo::addPoint(xmin, ymin, 0, hout, 441000);
    gmsh::model::geo::addPoint(xmax, ymin, 0, hout, 42);
    gmsh::model::geo::addPoint(xmax, ymax, 0, hout, 43);
    gmsh::model::geo::addPoint(xmin, ymax, 0, hout, 44);

    if (!axi) if (!line) gmsh::model::geo::addLine(441000, 42, 21);
    gmsh::model::geo::addLine(42,43,22);
    if (!line) gmsh::model::geo::addLine(43,44,23);
    gmsh::model::geo::addLine(44,441000,24);

    std::vector<int> outerSurfacePointLabels;
    std::vector<int> surfaceLabels(indices.size());
    int i = 0;
    for (auto ind = indices.begin(); ind != indices.end(); ++ind){
        gmsh::model::geo::addPoint(shellCoords[*ind][0], shellCoords[*ind][1], 0, h, i+100);
        i++;
    }

    if (axi && !line) {
        gmsh::model::geo::addLine(100, 42, 61);
        gmsh::model::geo::addLine(441000, i+99, 51);
    }

    for (int i=0; i < indices.size()-1; i++){
        gmsh::model::geo::addLine(i+100, i+101, i+100);
        surfaceLabels[i] = i+100;
    }

    if (!line) {
        gmsh::model::geo::addLine((indices.size() - 1) + 100, 100, (indices.size() - 1) + 100);
        surfaceLabels[indices.size() - 1] = (indices.size() - 1) + 100;
    }

    int const label = (indices.size()-1) +100;
    if (axi && !line) {
        std::vector<int> clVec;
        clVec.push_back(-51);
        clVec.push_back(-24);
        clVec.push_back(-23);
        clVec.push_back(-22);
        clVec.push_back(-61);
        for (int i = 0; i < surfaceLabels.size()-1; i++) {
            clVec.push_back(surfaceLabels[i]);
        }
        gmsh::model::geo::addCurveLoop(clVec, 1);
    } else if (!line) {
        gmsh::model::geo::addCurveLoop({21, 22, 23, 24}, 4); // outer boundary
    }
    if (!line) {
        gmsh::model::geo::addCurveLoop(surfaceLabels, 2); // (closed) surface
        gmsh::model::geo::addPlaneSurface({2}, 2);
    }
    if (axi && !line) {
        gmsh::model::geo::addPlaneSurface({1}, 1);
    } else if (!line) {
        gmsh::model::geo::addPlaneSurface({4,2}, 1); // outer boundary minus surface
    }
    //physical groups
    if (axi && !line) {
        gmsh::model::geo::addPhysicalGroup(1, {51,label,61}, 1);
    } else if (!line) {
        gmsh::model::geo::addPhysicalGroup(1, {21}, 1);
    }
    if (!line) {
        gmsh::model::geo::addPhysicalGroup(1, {24}, 4);
        gmsh::model::geo::addPhysicalGroup(1, {22}, 3);
        gmsh::model::geo::addPhysicalGroup(1, {23}, 5);

        gmsh::model::geo::addPhysicalGroup(2, {1}, 0); //external
        gmsh::model::geo::addPhysicalGroup(2, {2}, 1); //internal
    }

    if (line) {
        gmsh::model::geo::addLine(441000, 100, 61);
        gmsh::model::geo::addLine(100, 42, 51);
        gmsh::model::geo::addLine(i+99, 44, 41);
        gmsh::model::geo::addLine(i+99, 43, 31);

        std::vector<int> clVec, clVec2;
        clVec.push_back(61);
        clVec.push_back(24);
        clVec.push_back(41);
        for (int i = 0; i < surfaceLabels.size()-1; i++) {
            clVec.push_back(surfaceLabels[i]);
        }
        gmsh::model::geo::addCurveLoop(clVec, 2);

        clVec2.push_back(31);
        clVec2.push_back(-22);
        clVec2.push_back(-51);
        for (int i = 0; i < surfaceLabels.size()-1; i++) {
            clVec2.push_back(surfaceLabels[i]);
        }
        gmsh::model::geo::addCurveLoop(clVec2, 1);

        gmsh::model::geo::addPlaneSurface({2}, 2);
        gmsh::model::geo::addPlaneSurface({1}, 1);

        gmsh::model::geo::addPhysicalGroup(1, {51,61}, 1);
        gmsh::model::geo::addPhysicalGroup(1, {41,31}, 4);
        gmsh::model::geo::addPhysicalGroup(1, {22}, 3);
        gmsh::model::geo::addPhysicalGroup(1, {24}, 5);

        gmsh::model::geo::addPhysicalGroup(2, {1}, 0); //external
        gmsh::model::geo::addPhysicalGroup(2, {2}, 1); //internal
    }



    int field = Parameters::get<int>("size by field").value_or(0);
    if (field) {
        std::vector<double> surfaceLabelsPhi1, surfaceLabelsPhi2, surfaceLabelsPhi0, surfaceLabelsDist;
        int j=0;
        for (auto ind = indices.begin(); ind != indices.end(); ++ind){
            if (ind == indices.end()) break;
            auto phaseValue = phi.coefficients()[*ind];
            auto coords = shellCoords[*ind];
            auto coords2 = shellCoords[*std::next(ind)];
            auto dist = two_norm(coords2 - coords);
            if (phaseValue > 0.025 && phaseValue < 0.975) {
                surfaceLabelsPhi1.push_back(j+100);
            }
            if ((phaseValue < 0.025 || phaseValue > 0.975) && dist < 1.25*distMin) {
                surfaceLabelsDist.push_back(j+100);
            }
            if (phaseValue > 0.975 && !(dist < 1.25*distMin)) {
                surfaceLabelsPhi2.push_back(j+100);
            }
            if (phaseValue < 0.025 && !(dist < 1.25*distMin)) {
                if ((axi && j+1 != i) || !axi) surfaceLabelsPhi0.push_back(j+100);
            }
            j++;
        }
        // parameter default values are chosen s.t. the resulting grid looks good in most cases, however, the user can
        // choose parameters for his/her own liking
        double fieldMin = Parameters::get<double>("field distMin").value_or(distMin*1.5);
        double fieldMin0 = Parameters::get<double>("field distMin shell").value_or(distMaxSurface*1.5);
        double fieldMax = Parameters::get<double>("field distMax").value_or(distMax);
        double hPhase = Parameters::get<double>("mesh resolution at phase field interface").value_or(distMin*1.5);
        double hPhase1 = Parameters::get<double>("mesh resolution at shell phi 1").value_or(distMaxSurface);
        double hPhase0 = Parameters::get<double>("mesh resolution at shell phi 0").value_or(distMaxSurface);
        double hOutField = Parameters::get<double>("mesh resolution at boundary field").value_or(distMax);

        gmsh::model::mesh::field::add("Distance",1);
        gmsh::model::mesh::field::setNumbers(1, "CurvesList", surfaceLabelsPhi2); //fine grid where droplet is
        gmsh::model::mesh::field::setNumber(1, "Sampling", 100);

        gmsh::model::mesh::field::add("Distance",3);
        gmsh::model::mesh::field::setNumbers(3, "CurvesList", surfaceLabelsPhi1); //fine grid where droplet is
        gmsh::model::mesh::field::setNumber(3, "Sampling", 100);

        gmsh::model::mesh::field::add("Distance",5);
        gmsh::model::mesh::field::setNumbers(5, "CurvesList", surfaceLabelsPhi0); //fine grid where droplet is
        gmsh::model::mesh::field::setNumber(5, "Sampling", 100);

        gmsh::model::mesh::field::add("Threshold", 2);
        gmsh::model::mesh::field::setNumber(2, "InField", 1);
        gmsh::model::mesh::field::setNumber(2, "SizeMin", hPhase1);
        gmsh::model::mesh::field::setNumber(2, "SizeMax", hOutField);
        gmsh::model::mesh::field::setNumber(2, "DistMin", fieldMin);
        gmsh::model::mesh::field::setNumber(2, "DistMax", fieldMax);

        gmsh::model::mesh::field::add("Threshold", 4);
        gmsh::model::mesh::field::setNumber(4, "InField", 3);
        gmsh::model::mesh::field::setNumber(4, "SizeMin", hPhase);
        gmsh::model::mesh::field::setNumber(4, "SizeMax", hOutField);
        gmsh::model::mesh::field::setNumber(4, "DistMin", fieldMin);
        gmsh::model::mesh::field::setNumber(4, "DistMax", fieldMax);

        gmsh::model::mesh::field::add("Threshold", 6);
        gmsh::model::mesh::field::setNumber(6, "InField", 5);
        gmsh::model::mesh::field::setNumber(6, "SizeMin", hPhase0);
        gmsh::model::mesh::field::setNumber(6, "SizeMax", hOutField);
        gmsh::model::mesh::field::setNumber(6, "DistMin", fieldMin0);
        gmsh::model::mesh::field::setNumber(6, "DistMax", fieldMax);

        if (surfaceLabelsDist.size()>0) {
            gmsh::model::mesh::field::add("Distance",7);
            gmsh::model::mesh::field::setNumbers(7, "CurvesList", surfaceLabelsDist); //fine grid where droplet is
            gmsh::model::mesh::field::setNumber(7, "Sampling", 100);

            gmsh::model::mesh::field::add("Threshold", 8);
            gmsh::model::mesh::field::setNumber(8, "InField", 7);
            gmsh::model::mesh::field::setNumber(8, "SizeMin", (distMaxSurface + distMin)*0.25);
            gmsh::model::mesh::field::setNumber(8, "SizeMax", hOutField);
            gmsh::model::mesh::field::setNumber(8, "DistMin", fieldMin);
            gmsh::model::mesh::field::setNumber(8, "DistMax", fieldMax);

        }

        std::vector<double> fields = {2,4,6};
        if (surfaceLabelsDist.size()>0) fields.push_back(8);
        gmsh::model::mesh::field::add("Min", 700000);
        gmsh::model::mesh::field::setNumbers(700000,"FieldsList", fields);
        gmsh::model::mesh::field::setAsBackgroundMesh(700000);

        gmsh::option::setNumber("Mesh.MeshSizeExtendFromBoundary", 0);
        gmsh::option::setNumber("Mesh.MeshSizeFromPoints", 0);
        gmsh::option::setNumber("Mesh.MeshSizeFromCurvature", 0);
    }

    gmsh::model::geo::synchronize();

   // gmsh::option::setNumber("Mesh.Algorithm",8);
    gmsh::option::setNumber("Mesh.Smoothing",10);

    gmsh::write(meshDir + "/" + meshName + ".geo_unrolled");
    gmsh::model::mesh::generate(2);

    gmsh::option::setNumber("Mesh.MshFileVersion", 2.2);
    gmsh::write(meshDir + "/" + meshName + ".msh");

    gmsh::write(meshDir + "/backup/" + meshName + std::to_string(time) + ".geo_unrolled");
    gmsh::write(meshDir + "/backup/" + meshName + std::to_string(time) + ".msh");

    gmsh::finalize();
}

#endif //escrt_mvb_MESHGENERATOR_H