//
// Created by Marcel Mokbel on 30.11.2020.
//

#pragma once

#include <amdis/AMDiS.hpp>
#include "ShellForce.h"
#include "ShellGeometry.hpp"

using namespace AMDiS;

// Surface Tension Force to be computed on the surface and then mapped to the bulk domain
// GridView is that of the bulk domain, not that of the Surface grid
template <class ShellGeometry, class GridView>
class SurfaceTensionForce : public ShellForce {
    using GF = Dune::DiscreteGridViewFunction<GridView, GridView::dimension>;
public:
    SurfaceTensionForce(ShellGeometry& shellGeo, GridView const& gridView)
            :   shellGeo_(shellGeo)
    {
        Parameters::get("surfaceTension",gamma_);
        surfTen = std::make_unique<GF>(gridView, 1);
    }

    void updateForce(GridView const& gridView) {
        surfTen->update(gridView);
        Dune::Functions::interpolate(surfTen->basis(),
                                     surfTen->coefficients(),
                                     [](auto const& x) { return FieldVector<double,2>{0.0,0.0}; });

        shellGeo_.interpolateToBulk(surfTen->coefficients(), shellGeo_.surfaceNormal().coefficients(), -gamma_, shellGeo_.curvature().coefficients());
    }

    auto const& getForce(GridView const& gridView){
        updateForce(gridView);
        return *surfTen;
    }

private:
    ShellGeometry& shellGeo_;
    double gamma_;
    std::unique_ptr<GF> surfTen;
};
