//
// Created by Marcel Mokbel on 30.11.2020.
//

#pragma once

#include <algorithm>

#include <amdis/AMDiS.hpp>

#include <dune/grid/common/entity.hh>
#include "amdis/escrt_mvb/helperFunctions.hh"

using namespace AMDiS;

template<int dow>
struct CompareFieldVector {
    template<class T>
    static bool almost_equal(T x, T y, int ulp = 2) {
        return std::abs(x - y) <= std::numeric_limits<T>::epsilon() * std::abs(x + y) * ulp
               || std::abs(x - y) < std::numeric_limits<T>::min();
    }

    bool operator()(FieldVector<double, dow> const &lhs, FieldVector<double, dow> const &rhs) const {
        for (int i = 0; i < dow; i++) {
            if (almost_equal(lhs[i], rhs[i]))
                continue;
            return lhs[i] < rhs[i];
        }
        return false;
    }
};


/**
     * @brief extracts and returns the surface grid using the partitions vector
     * works at the moment only for 2D simplex mesh
     * creates a surface to fluid map mapping the indices on the new surface grid to the
     * respective indices on the bulk grid
     * creates an element vector normalsDirection storing vectors pointing in outward direction
     * for the computation of the outer unit normals on the surface
     *
     * @param partitions The vector containing element-wise partition indices.
     * @param coordinates grid function storing the coordinates of the grid points
     * @param surfaceToFluidMap map to be filled mapping surface grid indices to bulk grid indices
     * @param normalsDirection vector containing coordinates of a point outside of the shell for
     *                         each surface point
     */
static std::map<int,int> surfaceToFluidDefault;
static std::vector<FieldVector<double,2>> normalDirDefault;
template<class SurfaceGrid, class Coordinates>
auto createSurfaceMesh(std::vector<int> const &partitions,
                       Coordinates const &coordinates,
                       std::map<int, int> &surfaceToFluidMap = surfaceToFluidDefault,
                       std::vector<FieldVector<double,2>> &normalsDirection = normalDirDefault) {
    Dune::Timer t;
    auto const &basis = coordinates.basis();
    auto const &gridView = basis.gridView();
    auto const &level0GridView = gridView.grid().levelGridView(0);
    auto const &indexSet = level0GridView.indexSet();

    const int dow = SurfaceGrid::dimensionworld;

    Dune::GridFactory<SurfaceGrid> factory;
    std::array<unsigned int, dow> index;
    std::map<FieldVector<double, dow>, unsigned int, CompareFieldVector<dow>> visited;
    std::map<int, int> surfaceToFluidMapTmp;
    std::vector<FieldVector<double,dow>> normalsDirectionTmp;
    auto localView = basis.localView();
    for (auto const &e : elements(gridView)) {
        auto father = e;
        while (father.hasFather())
            father = father.father();
        int p = partitions[indexSet.index(father)];
        localView.bind(e);
        for (auto const &is : intersections(gridView, e)) {
            if (!is.neighbor())
                continue;
            auto fatherOutside = is.outside();
            while (fatherOutside.hasFather())
                fatherOutside = fatherOutside.father();
            int q = partitions[indexSet.index(fatherOutside)];
            if (p == 0 && q == 1) {
                int subEntityCodim = 1;
                int subEntityIndex = is.indexInInside(); // Local index of codim 1 entity in the inside() entity where intersection is contained in

                auto const &node = localView.tree().child(0);
                auto const &lfe = node.finiteElement();
                auto const &lc = lfe.localCoefficients();
                auto re = Dune::referenceElement(e);

                for (std::size_t i = 0, k = 0; i < lc.size() && k < 2; ++i) {
                    auto localKey = lc.localKey(i);
                    if (re.subEntities(subEntityIndex, subEntityCodim, localKey.codim()).contains(
                            localKey.subEntity())) {
                        unsigned int bulk_idx = localView.index(node.localIndex(i))[0];
                        FieldVector<double, dow> const &coord = coordinates.coefficients()[bulk_idx];

                        auto[it, inserted] = visited.emplace(coord, visited.size());
                        unsigned int surface_idx = it->second;

                        index[k++] = surface_idx;
                        if (inserted) {
                            // insert vertex and add indices to surfaceToFluidMap
                            factory.insertVertex(coord);
                            surfaceToFluidMapTmp.emplace(surface_idx, bulk_idx);
                        }
                    }
                }

                // compute elementwise vectors pointing outwards (for normals on surface)
                auto a = is.inside().geometry().center();
                auto b = is.geometry().center();
                auto outwardDirection = a - b;
                normalsDirectionTmp.push_back(outwardDirection);
                //insert element
                factory.insertElement(re.type(subEntityIndex, subEntityCodim), {index[0], index[1]});
            }
        }
    }

    std::unique_ptr<SurfaceGrid> surfaceGrid = factory.createGrid();

    // transform insertion index to real index
    surfaceToFluidMap.clear(); //empty the surfaceToFluidMap...
    auto const &surfaceIndexSet = surfaceGrid->leafIndexSet();
    for (auto const &v : vertices(surfaceGrid->leafGridView()))
        surfaceToFluidMap.emplace(surfaceIndexSet.index(v), surfaceToFluidMapTmp[factory.insertionIndex(v)]);

    normalsDirection.clear();
    for (auto const &el : elements(surfaceGrid->leafGridView()))
        normalsDirection.push_back(normalsDirectionTmp[factory.insertionIndex(el)]);

    AMDiS::info(2,"extractSurfaceMesh needed {} seconds", t.elapsed());
    return std::move(surfaceGrid);
}


/** @brief shell geometry class taking a previously extracted surface grid, which will be wrapped
 *         into a moving surface grid, representing the geometry of the surface,
 *         i.e. the coordinates of the surface DOFs and the surface normals and curvatures
 */
template<class SurfaceGrid>
class ShellGeometry {
    static const int dim = SurfaceGrid::dimension;
    static const int dow = SurfaceGrid::dimensionworld;

    using GridView = typename SurfaceGrid::LeafGridView;
    using CoordinatesGF = Dune::DiscreteGridViewFunction<GridView, dow>;
    using KappaGF = Dune::DiscreteGridViewFunction<GridView, 1>;
    using NormalsGF = Dune::DiscreteGridViewFunction<GridView, dow>;

public:
    /// @brief constructor creating the shell geometry by using the previously extracted surface mesh
    ShellGeometry(SurfaceGrid &surfaceGrid,
                  std::map<int, int> &surfaceToFluidMap,
                  std::vector<FieldVector<double,dow>> &elementNormalsDir)
            : surfaceToFluidMap_(surfaceToFluidMap)
            , coordinates_(surfaceGrid.leafGridView(), 1)
            , surfaceGrid_(wrap_or_share(surfaceGrid))
            , elementNormalsDirection_(elementNormalsDir)
            {

        kappaGF_ = std::make_unique<KappaGF>(surfaceGrid_->leafGridView(), 1);
        gaussianGF_ = std::make_unique<KappaGF>(surfaceGrid_->leafGridView(), 1);
        normalsGF_ = std::make_unique<NormalsGF>(surfaceGrid_->leafGridView(), 1);
        laplaceKappaGF_ = std::make_unique<KappaGF>(surfaceGrid_->leafGridView(), 1);

        //fill coordinates_
        updateGridFunctions(surfaceGrid.leafGridView(), surfaceToFluidMap);
    }

    /// @brief Return coordinate grid-function
    auto &coordinates() {
        return coordinates_;
    }

    /// @brief update DiscreteGridViewFunctions, to be called if the grid has changed
    template <class GridView>
    void updateGridFunctions(GridView gv, std::map<int,int> stfMap) {
        coordinates_.update(gv);
        kappaGF_->update(gv);
        gaussianGF_->update(gv);
        normalsGF_->update(gv);
        laplaceKappaGF_->update(gv);
        Dune::Functions::interpolate(coordinates_.basis(), coordinates_.coefficients(),
                                     [](auto const &x) { return x; });
        // update neighbor indices also
        nIdx_ = findNeighborIndices();
        surfaceToFluidMap_ = stfMap;
    }

    /** @brief create surface normals vector
     * calculate normals by finding the neighboring vertices of each vertex and then
     * using finite differences. The direction of the normals is determined by a
     * normalsDirection element vector which is computed within the createSurfaceMesh()
     * method by the knowledge of the outside element of the respective intersection
     * see also to Mokbel, 2020, "Aland An ALE method for simulations of axisymmetric elastic
     * surfaces in flow" * Int. J. Numerical Meth. in Fluids
     *
     * note: the use of the intersection's centerUnitOuterNormal() requires at least P2 elements
     *       and appears to be less accurate, so it's avoided here
     */
    auto const &surfaceNormal() {
        auto axi = Parameters::get<int>("axisymmetric").template value_or(0);
        auto &normals = normalsGF_->coefficients();
        auto const& indexSet = gridView().indexSet();

        std::vector<FieldVector<double,dow>> pointsOnSyxmmetryAxis;
        std::vector<int> idxOnSymmetryAxis;
        for (const auto& vertex : vertices(gridView())) {
            std::vector<FieldVector<double,dow>> neighbors;
            auto idx = indexSet.index(vertex);
            normals[idx] = FieldVector<double,dow>{0.0,0.0};
            auto midpoint = vertex.geometry().corner(0);

            // find neighbours of midpoint
            neighbors.push_back(coordinates_.coefficients()[nIdx_[idx][0]]);
            if (nIdx_[idx].size() == 2) neighbors.push_back(coordinates_.coefficients()[nIdx_[idx][1]]);

            // compute derivatives
            double Zs,Rs,Xs;
            computeDerivatives(neighbors,midpoint,Zs,Rs,Xs);

            // compute normals
            normals[idx][0] =  -Rs / Xs;
            normals[idx][1] =   Zs / Xs;

            // correct values at the poles
            if (axi && neighbors.size() == 1) {
                pointsOnSyxmmetryAxis.push_back(midpoint);
                idxOnSymmetryAxis.push_back(idx);
            }
            if (neighbors.size() == 1 && midpoint[1] < 1.e-5 && !axi) {
                normals[idx][0] = 1.0;
                normals[idx][1] = 0.0;
            }
        }
        if (axi) {
            if (pointsOnSyxmmetryAxis[0][0] < pointsOnSyxmmetryAxis[1][0]) {
                normals[idxOnSymmetryAxis[0]][0] = -1.0;
                normals[idxOnSymmetryAxis[0]][1] = 0.0;
                normals[idxOnSymmetryAxis[1]][0] = 1.0;
                normals[idxOnSymmetryAxis[1]][1] = 0.0;
            } else {
                normals[idxOnSymmetryAxis[0]][0] = 1.0;
                normals[idxOnSymmetryAxis[0]][1] = 0.0;
                normals[idxOnSymmetryAxis[1]][0] = -1.0;
                normals[idxOnSymmetryAxis[1]][1] = 0.0;
            }
        }
        return *normalsGF_;
    }


    /** @brief compute surface curvature vector
     * calculate curvatures (gaussian and mean) using using finite differences.
     * see also to Mokbel, 2020, "Aland An ALE method for simulations of axisymmetric elastic
     * surfaces in flow" * Int. J. Numerical Meth. in Fluids
     */
    auto const &curvature(bool writeGaussian = false) {
        auto &kappa = kappaGF_->coefficients();
        auto &K = gaussianGF_->coefficients();
        auto const& indexSet = gridView().indexSet();

        // go through all vertices
        for (const auto& vertex : vertices(gridView())) {
            std::vector<FieldVector<double,dow>> neighbors;
            auto idx = indexSet.index(vertex);
            kappa[idx] = 0.0;
            K[idx] = 0.0;
            auto midpoint = vertex.geometry().corner(0);

            // find neighbours of midpoint
            neighbors.push_back(coordinates_.coefficients()[nIdx_[idx][0]]);
            if (nIdx_[idx].size() == 2) neighbors.push_back(coordinates_.coefficients()[nIdx_[idx][1]]);

            // compute derivatives
            double Zs, Rs, Xs, Zss {}, Rss {};
            computeDerivatives(neighbors,midpoint,Zs,Rs,Xs, Zss, Rss);

            // 2D curvatures
            kappa[idx] =  (Rs * Zss - Rss * Zs) / Xs / Xs / Xs;
            K[idx] = 0.0; //kappa[idx]; //for 2D grids and a 1D curve, it is K = 0

            // add axisymmetric terms
            auto axi = Parameters::get<int>("axisymmetric").template value_or(0);
            if (axi) {
                kappa[idx] += Zs / Xs / (1e-15 + midpoint[1]);
                K[idx] = Zs * (Rs * Zss - Rss * Zs) / ((1e-15 + midpoint[1]) * std::pow(Xs, 4));
            }

            // zero curvature on each end of an open free shell
            // correct curvatures in axisymmetric case
            if (neighbors.size() < 2) {
                kappa[idx] = 0.0;
                K[idx] = 0.0;
                if (axi) { //for pillars: if not axi, but point touches bottom boundary
                    auto deltaZ = midpoint[0] - neighbors[0][0];
                    auto deltaR = midpoint[1] - neighbors[0][1];
                    if (midpoint[0] < 0) {
                        kappa[idx] = 4.0 * deltaZ / deltaR / sqrt(deltaZ * deltaZ + deltaR * deltaR);
                    } else {
                        kappa[idx] = -4.0 * deltaZ / deltaR / sqrt(deltaZ * deltaZ + deltaR * deltaR);
                    }
                    K[idx] = std::pow(kappa[idx],2) / 4.0 ;
                }
            }

        }
        if (writeGaussian) return *gaussianGF_;
        return *kappaGF_;
    }

    /** @brief compute surface laplacian of the curvature
     * expects previoulsy calculated curvature
     * see also to Mokbel, 2020, "Aland An ALE method for simulations of axisymmetric elastic
     * surfaces in flow" * Int. J. Numerical Meth. in Fluids
     */
    auto const &laplaceKappa() {
        auto &kappa = kappaGF_->coefficients();
        auto &laplaceKappa = laplaceKappaGF_->coefficients();
        auto const& indexSet = gridView().indexSet();

        for (const auto& vertex : vertices(gridView())) {
            auto idx = indexSet.index(vertex);
            std::vector<FieldVector<double,dow>> neighbors;
            laplaceKappa[idx] = 0.0;
            auto midpoint = vertex.geometry().corner(0);

            // find neighbours of midpoint
            neighbors.push_back(coordinates_.coefficients()[nIdx_[idx][0]]);
            if (nIdx_[idx].size() == 2) neighbors.push_back(coordinates_.coefficients()[nIdx_[idx][1]]);

             // compute distances between midpoint and neighbors
            double dist1, dist2;
            computeDist1Dist2(neighbors,midpoint, dist1,dist2);

            // compute laplaceKappa with 2nd order finite differences
            auto dist1dist2 = std::sqrt((neighbors[1] - neighbors[0]) * (neighbors[1] - neighbors[0]));
            if (neighbors.size() == 2) {
                laplaceKappa[idx] = ((kappa[nIdx_[idx][1]] - kappa[idx]) / dist1 - (kappa[idx] - kappa[nIdx_[idx][0]])
                        / dist2) / dist1dist2 *  2.0; /// Hss
            } else {
                if (midpoint[1] > 1e-5) {
                    laplaceKappa[idx] = 0.0;
                } else {
                    laplaceKappa[idx] = 0.0;//4.0 * (kappa[nIdx_[idx][0]] - kappa[idx]) / dist1;
                }
            }

            // add axisymmetric terms
            int axi = Parameters::get<int>("axisymmetric").value_or(0);
            if (axi) {
                if (neighbors.size() == 2) {
                    double Hs = (kappa[nIdx_[idx][1]] - (kappa[nIdx_[idx][0]])) / dist1dist2;
                    laplaceKappa[idx] += Hs * (neighbors[1][1] - neighbors[0][1]) / dist1dist2 / midpoint[1]; /// 1/r Hs rs
                } else {
                    laplaceKappa[idx] = 4.0 * (kappa[nIdx_[idx][0]] - kappa[idx]) / dist1;
                }
            }
        }
        return *laplaceKappaGF_;
    }

    /** @brief interpolation of values from surface to bulk
     * bulkVec = normalsVec * (factor * kappaVec)
     */
    template<class BulkVec, class NormalsVec, class CurvatureVec>
    void
    interpolateToBulk(BulkVec &bulkVec, NormalsVec const &normalsVec, double factor, CurvatureVec const &kappaVec) {
        for (auto it : surfaceToFluidMap_) {
            bulkVec[it.second] = normalsVec[it.first] * (factor * kappaVec[it.first]);
        }
    }

    /** @brief interpolation of values from surface to bulk
     * bulkVec = shellVec
     */
    template<class BulkVec, class ShellVec>
    void interpolateToBulk(BulkVec &bulkVec, ShellVec const &shellVec) {
        for (auto it : surfaceToFluidMap_) {
            bulkVec[it.second] = shellVec[it.first];
        }
    }

    /** @brief interpolation of values from bulk to surface
     */
    template<class BulkFct, class ShellFct>
    void interpolateToSurface(BulkFct const &bulkFct, ShellFct &shellFct) {
        for (auto it : surfaceToFluidMap_) {
            shellFct[it.first] = bulkFct[it.second];
        }
    }

    /// @brief return the surface grid
    auto const &surfaceGrid() const {
        return surfaceGrid_;
    }

    /// @brief return the mapping from surface indices to fluid indices
    auto &surfaceToFluidMap() const {
        return surfaceToFluidMap_;
    }

    /// @brief set the mapping from surface to fluid indices
    void setSurfaceToFluidMap(std::map<int, int> surfaceToFluidMap) {
        surfaceToFluidMap_.swap(surfaceToFluidMap);
    }

    /// @brief Return the gridView of the basis
    GridView gridView() const { return surfaceGrid_->leafGridView(); }

    /// @brief set the surface grid to "grid"
    template <class CD, class GV>
    void updateGrid(CD& coordsDiscrete, GV gridViewBulk, std::vector<int>const& partitions) {
        coordsDiscrete.update(gridViewBulk);
        coordsDiscrete = vertexCoordinates(gridViewBulk);
        auto grid = createSurfaceMesh<SurfaceGrid>(partitions,
                                                     coordsDiscrete,
                                                     surfaceToFluidMap_,
                                                     elementNormalsDirection_);

        std::shared_ptr<SurfaceGrid> temp(std::move(grid));
        surfaceGrid_.swap(temp);
        updateGridFunctions(gridView(), surfaceToFluidMap_); // also updates the coordinates
    }

    /// @brief return the element vector containing info about the direction, the normals should point to
    auto const& elementNormalsDirection() {
        return elementNormalsDirection_;
    }

    /// @brief return the index map containing neighbor indices for all vertices of the grid
    auto const& neighborIndices() {
        return nIdx_;
    }

    /// @brief compute the normal direction to a given point on the surface
    auto normalsDirection(FieldVector<double,dow> midpoint) {
        // find neighboring values of stExt, stExtOneEdge
        FieldVector<double,dow> normalDirection(0.0);
        auto const& indexSet = gridView().indexSet();
        for (const auto& el : elements(gridView())) {
            for (int i = 0; i < el.geometry().corners(); i++) {
                if ((el.geometry().corner(i) - midpoint).two_norm() < 1.e-5) {
                    normalDirection += elementNormalsDirection()[indexSet.index(el)];
                }
            }
        }
        return normalDirection;
    }

    /// @brief find neighbor indices for all vertices stored in a vector of vectors
    /// for each index, the result has one or two indices referring to the neighboring DOFs of the vertex
    auto findNeighborIndices() {
        auto const& indexSet = gridView().indexSet();
        std::vector<std::vector<int>> neighborIdx(coordinates_.coefficients().size());

        // go through all vertices
        for (const auto& v : vertices(gridView())) {
            FieldVector<double,dow> normalDirection(0.0);
            auto midpoint = v.geometry().corner(0);

            // find all elements containing the vertex
            for (const auto& el : elements(gridView())) {
                for (int i = 0; i < el.geometry().corners(); i++) {
                    if ((el.geometry().corner(i) - midpoint).two_norm() < 1.e-5) {
                        //compute element normal
                        auto t = el.geometry().corner(1) - el.geometry().corner(0);
                        auto n = t;
                        n[0] = -t[1];
                        n[1] = t[0];
                        if (n.dot(elementNormalsDirection()[indexSet.index(el)]) < 0) n=-n;
                        // store point on the outside grid to find if the indices have to be switched
                        normalDirection += n;

                        // go through all vertices to find the respective neighbor index
                        for (const auto& v2 : vertices(gridView())) {
                            if ((v2.geometry().corner(i) - el.geometry().corner((i+1)%2)).two_norm() < 1.e-5) {
                                neighborIdx[indexSet.index(v)].push_back(indexSet.index(v2));
                            }
                        }
                    }
                }
            }
            switchNeighbors(normalDirection,neighborIdx[indexSet.index(v)]);
        }
        return neighborIdx;
    }

    /// @brief switch the neighbors indices to correct the direction, the normals point into and to correct
    /// the computation of all surface properties, where derivatives have to be computed
    void switchNeighbors(FieldVector<double,2> normalDirection,
                         std::vector<int>& neighborIndices) {
        auto const& indexSet = gridView().indexSet();
        // go through all vertices
        for (const auto& v : vertices(gridView())) {
            std::vector<FieldVector<double,dow>> neighbors(2);
            if (neighborIndices.size() == 2) {
                neighbors[0] = coordinates_.coefficients()[neighborIndices[0]];
                neighbors[1] = coordinates_.coefficients()[neighborIndices[1]];
                auto midpoint = v.geometry().corner(0);

                // compute point distances
                double dist1, dist2;
                computeDist1Dist2(neighbors,midpoint,dist1,dist2);

                // compute normal
                double Zs = (neighbors[1][0] - neighbors[0][0]) / (dist1 + dist2);
                double Rs = (neighbors[1][1] - neighbors[0][1]) / (dist1 + dist2);
                double Xs = sqrt(Zs * Zs + Rs * Rs);
                auto auxNormal = FieldVector<double,2>{-Rs/Xs,Zs/Xs};

                // see, if direction of normal has to be switched and hence, neighborIndices have to be switched
                if (auxNormal.dot(normalDirection) < 0) {
                    auto aux = neighborIndices[1];
                    neighborIndices[1] = neighborIndices[0];
                    neighborIndices[0] = aux;
                }
            }
        }
    }

    /// @brief compute distances of a given midpoint to its neigboring point(s)
    void computeDist1Dist2(std::vector<Dune::FieldVector<double,2>> neighbors,
                           Dune::FieldVector<double,2> midpoint,
                           double &dist1,
                           double &dist2){
        FieldVector<double,2> d1;
        auto d2 = neighbors[0] - midpoint;
        (neighbors.size() == 2) ? d1 = neighbors[1] - midpoint : d1 = d2;
        dist1 = d1.two_norm();
        dist2 = d2.two_norm();
    }

    /// @brief compute the derivatives of the coordinates with respect to R and Z axis for a given midpoint
    void computeDerivatives(std::vector<Dune::FieldVector<double,2>> neighbors,
                            Dune::FieldVector<double,2> midpoint,
                            double &Zs,
                            double &Rs,
                            double &Xs,
                            std::optional<std::reference_wrapper<double>> Zss = {},
                            std::optional<std::reference_wrapper<double>> Rss = {}) {
        double dist1, dist2;
        computeDist1Dist2(neighbors,midpoint,dist1,dist2);
        Zs = (neighbors.size() == 2) ? (neighbors[1][0] - neighbors[0][0]) / (dist1 + dist2)
                : (neighbors[0][0] - midpoint[0]) / dist1;
        Rs = (neighbors.size() == 2) ? (neighbors[1][1] - neighbors[0][1]) / (dist1 + dist2)
                : (neighbors[0][1] - midpoint[1]) / dist1;
        Xs = sqrt(Zs * Zs + Rs * Rs);

        if (Zss) {
            Zss->get() = (neighbors.size() == 2) ? ((neighbors[1][0] - midpoint[0]) / dist1 -
                    (midpoint[0] - neighbors[0][0]) / dist2) / (dist1 + dist2) * 2.0
                            : 2.0* (neighbors[0][0] - midpoint[0]) / dist1/dist1;
            Rss->get() = (neighbors.size() == 2) ? ((neighbors[1][1] - midpoint[1]) / dist1 -
                    (midpoint[1] - neighbors[0][1]) / dist2) / (dist1 + dist2) * 2.0
                            : 2.0* (neighbors[0][1] - midpoint[1]) / dist1 /dist1;
        }
    }

public:
    std::map<int, int> &surfaceToFluidMap_;
    CoordinatesGF coordinates_;
    std::shared_ptr<SurfaceGrid> surfaceGrid_;
    std::vector<FieldVector<double,dow>> &elementNormalsDirection_; // one coordinate per element for finding outward direction of normal
    std::vector<std::vector<int>> nIdx_;                            // indices of neigbor DOFs for each vertex
    std::unique_ptr<KappaGF> kappaGF_;
    std::unique_ptr<KappaGF> gaussianGF_;
    std::unique_ptr<NormalsGF> normalsGF_;
    std::unique_ptr<KappaGF> laplaceKappaGF_;
};
