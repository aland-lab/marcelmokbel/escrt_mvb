//
// Created by Marcel Mokbel on 30.11.2020.
//

#pragma once

#include <amdis/AMDiS.hpp>
#include "ShellForce.h"
#include "ShellGeometry.hpp"

using namespace AMDiS;

// Stretching Force to be computed on the surface and then mapped to the bulk domain
// GridView is that of the bulk domain, not that of the Surface grid
template <class ShellGeometry, class GridView>
class StretchingForceAxi : public ShellForce {
    using GridViewSurface = SurfaceGrid::LeafGridView;
    using GF = Dune::DiscreteGridViewFunction<GridView, GridView::dimensionworld>;
public:
    StretchingForceAxi(ShellGeometry& shellGeo,
                       GridView const& gridView,
                       std::vector<int> partitions)
    : shellGeo_(shellGeo)
    , gridView_(gridView)
    , gridViewSurface_(shellGeo_.gridView())
    , partitions_(partitions){
        Parameters::get("areaDilation", Ka_);
        Parameters::get("areaShear", Ks_);
        stretchingForce_ = std::make_unique<GF>(gridView, 1);
    }

    void updateForce(GridView const& gridView) {
        auto kappa = shellGeo_.curvature().coefficients();
        auto normals = shellGeo_.surfaceNormal().coefficients();
        auto coords = shellGeo_.coordinates().coefficients();
        auto stretchingForceShell = normals;

        std::vector<double> surfaceTension(kappa.size()),
                            surfaceTension2d(kappa.size()),
                            surfaceTensionGrad(kappa.size());
        calcElasticEnergyTerms(surfaceTension,surfaceTension2d,surfaceTensionGrad);

        auto axi = Parameters::get<int>("axisymmetric").value_or(0);
        for (auto const& v : vertices(gridViewSurface_)) {
            auto idx = gridViewSurface_.indexSet().index(v);
            if (axi && coords[idx][1] < 1e-5) {
                stretchingForceShell[idx][0] = 0.0;
                stretchingForceShell[idx][1] = 0.0;
            } else {
                stretchingForceShell[idx][0] = (-kappa[idx] * surfaceTension[idx] * normals[idx][0]
                        + surfaceTensionGrad[idx] * normals[idx][1]); //note: normals[i][1] = -tangential[i][0]

                stretchingForceShell[idx][1] = (-kappa[idx] * surfaceTension[idx] * normals[idx][1]
                        - surfaceTensionGrad[idx] * normals[idx][0])  //note: normals[i][0] = tangential[i][1]
                                + axi * 1.0 / (coords[idx][1]+1.e-10) * surfaceTension2d[idx];
            }
        }

        // update stretchingForce to adapted grid
        stretchingForce_->update(gridView);
        Dune::Functions::interpolate(stretchingForce_->basis(),
                                     stretchingForce_->coefficients(),
                                     [](auto const& x) { return FieldVector<double,2>{0.0,0.0}; });

        shellGeo_.interpolateToBulk(stretchingForce_->coefficients(), stretchingForceShell);
    }

    auto const& getForce(GridView const& gridView){
        updateForce(gridView);
        return *stretchingForce_;
    }

    // Calculate principal stretches \lambda_1^and \lambda_2 of the membrane.
    // for gradients containing \lambda_1, we take changes only with respect to a single facet in
    // \lambda1OneEgde
    void calcPrincipalStretches(std::vector<double> &lambda1,
                                std::vector<double> &lambda2,
                                std::vector<double> &lambda1OneEdge) {
        // extract gridView of deformed/undeformed grid
        gridViewSurface_ = shellGeo_.gridView();
        auto grid = undeformedSurfaceGrid();
        auto gridViewUndeformed = grid->leafGridView();

        // extract coordinates of (un)deformed grid
        Dune::Functions::interpolate(shellGeo_.coordinates().basis(), shellGeo_.coordinates().coefficients(),
                                     [](auto const &x) { return x; });
        auto coords = shellGeo_.coordinates().coefficients();
        auto coordsOrig = Dune::DiscreteGridViewFunction<GridViewSurface>(gridViewUndeformed);
        Dune::Functions::interpolate(coordsOrig.basis(), coordsOrig.coefficients(),
                                     [](auto const &x) { return x; });

        // create the neighboring index map for indices of neighbors to each vertex
        auto nIdx = shellGeo_.neighborIndices();
        auto const& indexSet = gridViewSurface_.indexSet();

        // loop over all vertices on the deformed grid
        for (const auto& vertex : vertices(gridViewSurface_)) {
            auto idx = indexSet.index(vertex);

            // midpoint(Undeformed) is the vertex we are interested in, neighbors stores the neighbor coords
            auto midpoint = coords[idx];
            auto midpointUndeformed = coordsOrig.coefficients()[idx];
            std::vector<FieldVector<double,2>> neighbors;
            std::vector<FieldVector<double,2>> neighborsUndeformed;

            // find neighbours of midpoint and midpointUndeformed
            neighbors.push_back(coords[nIdx[idx][0]]);
            if (nIdx[idx].size() == 2) neighbors.push_back(coords[nIdx[idx][1]]);
            neighborsUndeformed.push_back(coordsOrig.coefficients()[nIdx[idx][0]]);
            if (nIdx[idx].size() == 2) neighborsUndeformed.push_back(coordsOrig.coefficients()[nIdx[idx][1]]);

            // compute distances between midpoint and neighbors
            double dist1, dist2, dist1Undeformed, dist2Undeformed;
            shellGeo_.computeDist1Dist2(neighbors, midpoint,dist1, dist2);
            shellGeo_.computeDist1Dist2(neighborsUndeformed, midpointUndeformed, dist1Undeformed, dist2Undeformed);

            //changes lambdas initially if only the elastic force should be tested (else: set to 1.0)
            auto elasticOnly = Parameters::get<double>("test for elastic only").value_or(1.0);

            // compute lambda1 and lambda2 and lambda1OneEdge
            lambda1[idx] = elasticOnly * (dist1 + dist2) / (dist1Undeformed + dist2Undeformed) - 1.0;  // longitudinal stretching
            lambda2[idx] = midpoint[1] / (midpointUndeformed[1] + 1.e-10)  - 1.0; // rotational stretching
            lambda1OneEdge[idx] = elasticOnly * dist1 / dist1Undeformed - 1.0;

            // correct values at the poles
            auto axi = Parameters::get<int>("axisymmetric").value_or(0);
            if ((axi && coords[idx][1] < 1.e-5) || neighbors.size() < 2) {
                lambda1[idx] = elasticOnly * dist1 / dist1Undeformed - 1.0;
                lambda2[idx] = lambda1[idx];
            }
            if (!axi) lambda2[idx] = 0.0;
        }


    }

    // calculate stretching force components according to
    // Mokbel et. al. (2020). An ALE Method for axisymmetric simulations of elastic surfaces in flow
    void calcElasticEnergyTerms(std::vector<double> &st,
                                std::vector<double> &st2d,
                                std::vector<double> &stGrad) {
        auto coords = shellGeo_.coordinates().coefficients();
        int numVertices = coords.size();

        std::vector<double> lambda1(numVertices), lambda2(numVertices), lambda1OneEdge(numVertices);
        calcPrincipalStretches(lambda1,lambda2,lambda1OneEdge);
        /// calculate effective surfaceTension and gradient of surfaceTension
        std::vector<double> stExt(numVertices), stExtOneEdge(numVertices);

        for (int i = 0; i < numVertices; i++) {
            st[i] = (Ka_ + Ks_) * lambda1[i] + (Ka_ - Ks_) * lambda2[i];   /// the effective surface tension due to membrane elasticity
            st2d[i] = (2.0 * Ks_) * (lambda1[i] - lambda2[i]); /// used for the force in radial direction
            stExt[i] = (Ka_ - Ks_) * lambda2[i];  /// an extension of surfaceTension to calculate a gradient of surfaceTension
            stExtOneEdge[i] = (Ka_ + Ks_) * lambda1OneEdge[i];
        }

        // create the neighboring index map for indices of neighbors to each vertex
        auto nIdx = shellGeo_.neighborIndices();
        auto const& indexSet = gridViewSurface_.indexSet();

        // loop over all vertices
        for (const auto& vertex : vertices(gridViewSurface_)) {
            auto idx = indexSet.index(vertex);
            std::vector<FieldVector<double,2>> neighbors;
            auto midpoint = vertex.geometry().corner(0);

            // find neighbors of midpoint
            neighbors.push_back(coords[nIdx[idx][0]]);
            if (nIdx[idx].size() == 2) neighbors.push_back(coords[nIdx[idx][1]]);

            // compute distances from midpoint to the neighboring coordinates
            double dist1, dist2;
            shellGeo_.computeDist1Dist2(neighbors,midpoint,dist1, dist2);

            // compute stGrad!
            if (neighbors.size() == 2) {
                stGrad[idx] = (stExt[nIdx[idx][1]] - stExt[nIdx[idx][0]]) / (dist1 + dist2) +
                        (stExtOneEdge[idx] - stExtOneEdge[nIdx[idx][0]]) / (dist1 + dist2) * 2.0;
            } else {
                // correct values at the poles
                stGrad[idx] = 0.0;
                auto axi = Parameters::get<int>("axisymmetric").value_or(1);
                auto elasticOnly = Parameters::get<double>("test for elastic only").value_or(1.0);
                if (!axi) {
                    //for free point: include ghost point on which lambda1 = 0, hence, dist2 = dist1Undeformed
                    auto dist1Undeformed = elasticOnly * dist1 / (lambda1[idx] + 1.0);
                    stGrad[idx] =
                            (stExtOneEdge[idx] - 0.0) / (dist1 + dist1Undeformed) * 2.0;
                    if (midpoint[1] < 1e-5) stGrad[idx] = 0.0;
                }
            }
        }
    }

    // Create surface mesh of undeformed grid
    auto undeformedSurfaceGrid() {
        // create a special (discrete) gridFunction with internal storage of coefficients
        Dune::DiscreteGridViewFunction coordinatesUndeformedBulk{
            gridView_.grid().hostGrid()->hostGrid().hostGrid()->leafGridView(), 1};

        // interpolate the coordinate function into this gridFunction container
        Dune::Functions::interpolate(coordinatesUndeformedBulk.basis(),
                                     coordinatesUndeformedBulk.coefficients(),
                                     [](auto const& x) { return x; });

        std::unique_ptr surfaceGridUndeformed = createSurfaceMesh<SurfaceGrid>(partitions_,
                                                                               coordinatesUndeformedBulk);
        return std::move(surfaceGridUndeformed);
    }

private:
    ShellGeometry& shellGeo_;
    double Ka_, Ks_;
    std::unique_ptr<GF> stretchingForce_;
    std::vector<int> partitions_;
    GridView const& gridView_;
    SurfaceGrid::LeafGridView gridViewSurface_;
};
