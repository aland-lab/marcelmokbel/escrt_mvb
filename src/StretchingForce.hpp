//
// Created by Marcel Mokbel on 30.11.2020.
//

#pragma once

#include <amdis/AMDiS.hpp>
#include "ShellForce.h"
#include "ShellGeometry.hpp"
#include <amdis/interpolators/LocalAverageInterpolator.hpp>

using namespace AMDiS;

// Stretching Force to be computed on the surface and then mapped to the bulk domain
// GridView is that of the bulk domain, GridViewSurface is that of the Surface grid
template <class ShellGeometry, class GridView>
class StretchingForce : public ShellForce {
    using GridViewSurface = SurfaceGrid::LeafGridView;
    using GF = Dune::DiscreteGridViewFunction<GridView, GridView::dimensionworld>;
    using GFS = Dune::DiscreteGridViewFunction<GridViewSurface, 1>;
public:
    StretchingForce(ShellGeometry& shellGeo,
                    GridView const& gridView,
                    std::vector<int> partitions)
            :   shellGeo_(shellGeo)
            , gridView_(gridView)
            , gridViewSurface_(AdaptiveGrid<SurfaceGrid>(shellGeo_.surfaceGrid()).leafGridView())
            , partitions_(partitions) {

        Parameters::get("areaDilation", Ka_);
        Parameters::get("areaShear", Ks_);
        stretchingForce_ = std::make_unique<GF>(gridView, 1);
        double elasticOnly = 1.0;
        Parameters::get("test for elastic only", elasticOnly);  //changes lambdas initially if only the elastic force should be tested
    }

    // compute the actual bending force
    void updateForce(GridView const& gridView) {
        AdaptiveGrid<SurfaceGrid> grid(shellGeo_.surfaceGrid());
        gridViewSurface_ = grid.leafGridView();
        GlobalBasis basis(gridViewSurface_,lagrange<1>());
        GlobalBasis basis2(gridViewSurface_,power<2>(lagrange<1>()));
        auto normals = shellGeo_.surfaceNormal().coefficients();
        auto stretchingForceShell = shellGeo_.surfaceNormal(); // just to have the desired data type

        // get necessary values of B, P and trace(B-P)
        auto B = leftCauchyGreenStrain();
        auto P = projection();
        auto traceBP = std::make_shared<DOFVector<decltype(basis)>>(basis);
        for (int i = 0; i < dow; i++) {
            valueOf(*traceBP) += (valueOf(*B[i][i])-valueOf(*P[i][i]));
        }

        // compute stretching force tensor
        Dune::FieldMatrix<std::shared_ptr<DOFVector<decltype(basis)>>,2,2> stretchingForce;
        for (int i = 0; i < dow; i++){
            for (int j = 0; j < dow; j++) {
                stretchingForce[i][j] = std::make_shared<DOFVector<decltype(basis)>>(basis);
                valueOf(*stretchingForce[i][j]) << Ks_*(valueOf(*B[i][j]) - valueOf(*P[i][j]))
                                                + 0.5*(Ka_-Ks_)*valueOf(*traceBP)*valueOf(*P[i][j]);
            }
        }

        // actual force is divergence of stretchingForce tensor
        auto divS = std::make_shared<DOFVector<decltype(basis2)>>(basis2);
        for (int i = 0; i < dow; i++) {
            for (int j = 0; j < dow; j++) {
                //valueOf(*divS,i).interpolate(valueOf(*divS,i)
                //        + partialDerivativeOf(valueOf(*stretchingForce[i][j]),j),tag::local_average{});
                Dune::DiscreteGridViewFunction<decltype(gridViewSurface_),1,1,double> grad(gridViewSurface_);
                auto gradGF = makeGridFunction(partialDerivativeOf(valueOf(*stretchingForce[i][j]),j),gridViewSurface_);
                Dune::Functions::interpolate(grad.basis(),
                                             grad.coefficients(),
                                             gradGF);
                valueOf(*divS,i) += grad;
            }
        }

        // update stretchingForce to adapted grid
        stretchingForce_->update(gridView);
        Dune::Functions::interpolate(stretchingForce_->basis(),
                                     stretchingForce_->coefficients(),
                                     [](auto const& x) { return FieldVector<double,2>{0.0,0.0}; });

        // map values from DOFVector to DiscreteGridViewFunction
        auto const& indexSet = gridViewSurface_.indexSet();
        for (auto const& v : vertices(gridViewSurface_)) {
            auto idx = indexSet.index(v);
            stretchingForceShell.coefficients()[idx][0] = valueOf(*divS,0)(v.geometry().corner(0));
            stretchingForceShell.coefficients()[idx][1] = valueOf(*divS,1)(v.geometry().corner(0));
        }
        // map values from surface to bulk
        shellGeo_.interpolateToBulk(stretchingForce_->coefficients(), stretchingForceShell.coefficients());
    }

    auto const& getForce(GridView const& gridView){
        updateForce(gridView);
        return *stretchingForce_;
    }

    // Create surface mesh of undeformed grid
    auto undeformedSurfaceGrid() {
        // create a special (discrete) gridFunction with internal storage of coefficients
        Dune::DiscreteGridViewFunction coordinatesUndeformedBulk{
            gridView_.grid().hostGrid()->hostGrid().hostGrid()->leafGridView(), 1};

        // interpolate the coordinate function into this gridFunction container
        Dune::Functions::interpolate(coordinatesUndeformedBulk.basis(),
                                     coordinatesUndeformedBulk.coefficients(),
                                     [](auto const& x) { return x; });

        std::unique_ptr surfaceGridUndeformed = createSurfaceMesh<SurfaceGrid>(partitions_,
                                                                               coordinatesUndeformedBulk);
        return std::move(surfaceGridUndeformed);
    }

    // compute the left Cauchy-Green strain tensor using coordinates in undeformed and deformed state
    auto leftCauchyGreenStrain() {
        AdaptiveGrid<SurfaceGrid> grid(undeformedSurfaceGrid());
        auto gridViewUndeformed = grid.leafGridView();
        GlobalBasis basis(gridViewSurface_,lagrange<1>());
        GlobalBasis basisUndeformed(gridViewSurface_,lagrange<1>());
        GlobalBasis basisUndeformed2(gridViewUndeformed,power<2>(lagrange<1>()));

        // coordinates DOFVector
        auto coords = std::make_shared<DOFVector<decltype(basisUndeformed2)>>(basisUndeformed2);

        // DiscreteGridViewFunctions for undeformed and deformed grid
        Dune::DiscreteGridViewFunction coordsUndeformedDiscrete{gridViewUndeformed, 1};
        Dune::DiscreteGridViewFunction coordsDiscrete{gridViewSurface_, 1};
        Dune::Functions::interpolate(coordsDiscrete.basis(),
                                     coordsDiscrete.coefficients(),
                                     [](auto const& x) { return x; });

        // set the values on the undeformed grid to the coordinate values of the deformed grid
        coordsUndeformedDiscrete.coefficients() = coordsDiscrete.coefficients();
        valueOf(*coords) << coordsUndeformedDiscrete;


        // compute the gradient F = \nabla_x X, where x -> coords in undedormed state, X -> coords in deformed state
        // compute the left Cauchy Green strain FF^T
        // B ... Cauchy-Green strain tensor on undeformed grid...has to be transformed to new grid by assignment per DOF
        // B_new ... Cauchy-Green stain on deformed grid
        using Basis = decltype(basis);
        Dune::FieldMatrix<std::shared_ptr<DOFVector<Basis>>,2,2> B, B_new;
        Dune::DiscreteGridViewFunction<decltype(gridViewUndeformed),1,1,double> gradProduct(gridViewUndeformed);
        for (int i = 0; i < dow; i++) {
            for (int j = 0; j < dow; j++) {
                B[i][j] = std::make_shared<DOFVector<Basis>>(basisUndeformed);
                B_new[i][j] = std::make_shared<DOFVector<Basis>>(basis);
                for (int l = 0; l < dow; l++) {
                   // valueOf(*B[i][j]).interpolate(valueOf(*B[i][j]) + partialDerivativeOf(valueOf(*coords,i),l)
                   // * partialDerivativeOf(valueOf(*coords,l),j),tag::local_average{});
                   auto grad_il_grad_lj = makeGridFunction(partialDerivativeOf(valueOf(*coords,i),l)
                            * partialDerivativeOf(valueOf(*coords,l),j), gridViewUndeformed);
                   Dune::Functions::interpolate(gradProduct.basis(), gradProduct.coefficients(), grad_il_grad_lj);
                   valueOf(*B[i][j]) << valueOf(*B[i][j]) + gradProduct;
                }
                transformToNewGrid(*B[i][j],*B_new[i][j]);
            }
        }

        return B_new;
    }

    // compute projection onto the tangent plane
    auto projection() {
        auto normal = shellGeo_.surfaceNormal();
        GlobalBasis basis(gridViewSurface_,lagrange<1>());
        GlobalBasis basis2(gridViewSurface_,power<2>(lagrange<1>()));
        // normals DOFVector
        auto n = std::make_shared<DOFVector<decltype(basis2)>>(basis2);
        valueOf(*n) << normal;

        // compute the projection P = I - nn^T
        using Basis = decltype(basis);
        Dune::FieldMatrix<std::shared_ptr<DOFVector<Basis>>,2,2> P;
        for (int i = 0; i < dow; i++) {
            for (int j = 0; j < dow; j++) {
                P[i][j] = std::make_shared<DOFVector<Basis>>(basis);
                valueOf(*P[i][j]) << (i == j) - valueOf(*n,i) * valueOf(*n,j);
            }
        }
        return P;
    }

    // transform data from DOFVector on undeformed grid to deformed grid by index (no interpolation!)
    template<class Basis>
    void transformToNewGrid(DOFVector<Basis> const& dataOnOldGrid, DOFVector<Basis>& dataOnNewGrid) {
        auto localView = dataOnOldGrid.basis().localView();
        GlobalBasis basis(gridViewSurface_,lagrange<1>());
        for (const auto& e : elements(dataOnOldGrid.basis().gridView())) {
            localView.bind(e);
            for (std::size_t i = 0; i < localView.tree().size(); ++i) {
                auto value = dataOnOldGrid.get(localView.index(i));
                dataOnNewGrid.set(localView.index(i),value);
            }
        }
        dataOnNewGrid.finish(); // finish value insertion
    }

private:
    ShellGeometry& shellGeo_;
    double Ka_, Ks_;
    std::unique_ptr<GF> stretchingForce_;
    AdaptiveGrid<SurfaceGrid>::LeafGridView gridViewSurface_;
    GridView const& gridView_;
    std::vector<int> partitions_;
    const int dow = GridView::dimensionworld;
};
