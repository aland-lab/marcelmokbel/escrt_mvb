//
// Created by Marcel Mokbel on 30.11.2020.
//

#pragma once

#include <amdis/AMDiS.hpp>
#include "ShellForce.h"
#include "ShellGeometry.hpp"

using namespace AMDiS;
using SurfaceGrid = Dune::FoamGrid<1,2>;

// Bending Force to be computed on the surface and then mapped to the bulk domain
// GridView is that of the bulk domain, GridViewSurface is that of the Surface grid
template <class ShellGeometry, class GridView, class GridViewSurface>
class BendingForce : public ShellForce {
    using GF = Dune::DiscreteGridViewFunction<GridView, GridView::dimensionworld>;
    using GFS = Dune::DiscreteGridViewFunction<GridViewSurface, 1>;
public:
    BendingForce(ShellGeometry& shellGeo,
                 GridView const& gridView,
                 GridViewSurface const& gridViewSurface,
                 std::vector<int> partitions)
            :   shellGeo_(shellGeo)
            , gridViewSurface_(gridViewSurface)
            , gridView_(gridView)
            , partitions_(partitions)
            , kappaRef_(gridViewSurface,1)
            , laplaceKappaRef_(gridViewSurface,1){
        Parameters::get("bendingStiffness", bendingModulus_);
        Parameters::get("zeroReferenceCurvature", zeroReferenceCurvature);
        bendingForce_ = std::make_unique<GF>(gridView, 1);

        auto kappa = shellGeo_.curvature().coefficients();
        auto laplaceKappa = shellGeo_.laplaceKappa().coefficients();
        kappaRef_.coefficients() = kappa;
        laplaceKappaRef_.coefficients() = laplaceKappa;
        if (zeroReferenceCurvature) {
            for (int i = 0; i < kappa.size(); i++) {
                kappaRef_.coefficients()[i] = 0.0;
                laplaceKappaRef_.coefficients()[i] = 0.0;
            }
        }
    }

    void updateForce(GridView const& gridView) {
        auto kappa = shellGeo_.curvature().coefficients();
        auto laplaceKappa = shellGeo_.laplaceKappa().coefficients();
        auto gaussian = shellGeo_.curvature(true).coefficients();
        auto bendingForceShell = kappa;
        for (int i = 0; i < kappa.size(); ++i) {
            bendingForceShell[i] =
                    laplaceKappa[i] + (kappa[i] * kappa[i] - 2.0 * gaussian[i]) * kappa[i]
                    - 0.5 * pow(kappa[i],3);
        }
        bendingForce_->update(gridView);
        Dune::Functions::interpolate(bendingForce_->basis(),
                                     bendingForce_->coefficients(),
                                     [](auto const& x) { return FieldVector<double,2>{0.0,0.0}; });

        shellGeo_.interpolateToBulk(bendingForce_->coefficients(),
                                    shellGeo_.surfaceNormal().coefficients(),
                                    bendingModulus_, bendingForceShell);
    }

    auto& getForce(GridView const& gridView){
        updateForce(gridView);
        return *bendingForce_;
    }

    std::vector<double> getKappaRef() {
        std::vector<double> result(kappaRef_.coefficients().size());
        for (int i = 0; i < kappaRef_.coefficients().size(); i++) {
            result[i] = kappaRef_.coefficients()[i];
        }
        return result;
    }

    void setKappaRef(std::vector<double>& kappaRef) {
        for (int i = 0; i < kappaRef_.coefficients().size(); i++) {
            kappaRef_.coefficients()[i] = kappaRef[i];
        }
    }

    std::vector<double> getLaplaceKappaRef() {
        std::vector<double> result(laplaceKappaRef_.coefficients().size());
        for (int i = 0; i < laplaceKappaRef_.coefficients().size(); i++) {
            result[i] = laplaceKappaRef_.coefficients()[i];
        }
        return result;
    }

    void setLaplaceKappaRef(std::vector<double>& laplKappaRef) {
        for (int i = 0; i < laplaceKappaRef_.coefficients().size(); i++) {
            laplaceKappaRef_.coefficients()[i] = laplKappaRef[i];
        }
    }

    void setIdxOrig(std::vector<int> idx) {
        idxOrig_ = idx;
    }

    void setOriginalData(std::vector<int> idx,
                         std::vector<double> kappaRef,
                         std::vector<double> laplaceKappaRef) {
        setIdxOrig(idx);
        setKappaRef(kappaRef);
        setLaplaceKappaRef(laplaceKappaRef);
    }

private:
    ShellGeometry& shellGeo_;
    double bendingModulus_;
    int zeroReferenceCurvature;
    std::unique_ptr<GF> bendingForce_;
    GFS kappaRef_, laplaceKappaRef_;
    GridViewSurface gridViewSurface_;
    GridView const& gridView_;
    std::vector<int> idxOrig_;
    std::vector<int> partitions_;
};
