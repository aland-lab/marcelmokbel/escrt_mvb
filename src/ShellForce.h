//
// Created by Marcel Mokbel 2020
//
#pragma once

#include <amdis/AMDiS.hpp>

using namespace AMDiS;

class ShellForce {
public:
    // todo: return expression and overloading () operator
    auto const& getForce();
};
