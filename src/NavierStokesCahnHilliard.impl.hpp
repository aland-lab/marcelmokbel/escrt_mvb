/**
 * Implementation of the Navier Stokes problem
 * Therefore, we use the AMDiS::BaseProblem
 * You'll need the amdis-extensions module for that
 **/

#pragma once

#include <amdis/Integrate.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/localoperators/StokesOperator.hpp>
#include <amdis/surfacebasis/SurfaceLocalOperators.hpp>
#include "MeshMovementBC.hpp"
#include <amdis/datatransfers/NoDataTransfer.hpp>
#include <amdis/datatransfers/SimpleDataTransfer.hpp>
#include <amdis/escrt_mvb/PointIntegralOperatorAxi.hpp>
#include <amdis/escrt_mvb/PointIntegralOperatorAxiKappa.hpp>
#include <amdis/escrt_mvb/AxisymmetricDirichletBC.hpp>
#include <amdis/escrt_mvb/SurfaceZeroOrderTestVecNormal.hpp>
#include <amdis/escrt_mvb/SurfaceZeroOrderTestVecTrialSpecial.hpp>
#include <amdis/escrt_mvb/SurfaceFirstOrderTestDivTrialvecAxi.hpp>
#include <amdis/escrt_mvb/SurfaceZeroOrderTestTrialAxi2.hpp>

namespace AMDiS {

    template <class HostGrid, class PBF>
    NavierStokesCahnHilliard<HostGrid, PBF>::NavierStokesCahnHilliard(std::string const& name
                                              , HostGrid& grid
                                              , PBF& pbf
                                              , std::vector<int> boundaryIDs
                                              , std::vector<int> partitions)
        : ProblemIterationInterface()
        , ProblemTimeInterface()
        , ProblemInstatBase(name)
        , problem_(name + "->space", grid, pbf)
        , name_(name)
        , boundaryIDs_(boundaryIDs)
        , partitions_(partitions)
        , partitionsVec(grid,-1)
        , elementSizes(grid,-1)
        , elementLevels(grid,-1)
    {
        // set the correct boundary IDs to the mesh
        problem().boundaryManager()->setBoundaryIds(boundaryIDs_);
        Parameters::get("stokes->density", rho_);
        Parameters::get("stokes->density2", rho2_);
        Parameters::get("stokes->densityOutside", rhoOut_);
        Parameters::get("stokes->viscosity", nu_);
        Parameters::get("axisymmetric", axi);
        Parameters::get("parameters->eps", eps_);
        Parameters::get("parameters->sigma", sigma_);
        sigma_ = 6.0*sqrt(2)*sigma_; //rescale physically correct sigma_ to model sigma
        mobility_ = Parameters::get<double>("parameters->M0").value_or(1.0);
        permeability_ = Parameters::get<double>("parameters->permeability").template value_or(0.0);
    }

    template <class HostGrid, class PBF>
    void NavierStokesCahnHilliard<HostGrid, PBF>::solveInitialProblem(AdaptInfo& adaptInfo) {
        msg("NavierStokesCahnHilliard::solveInitialProblem");

        double eps = Parameters::get<double>("parameters->eps").value_or(0.1);
        auto phase = [eps](auto s)
        {
            return 0.5*(1.0 - invokeAtQP(Operation::Tanh{}, evalAtQP(s)*(1.0/(4.0*std::sqrt(2.0)*eps))));
        };

        // indicator function to indicate, that the phase field only exists above a threshold
        auto elementVectorIn = ElementVector(*grid(),0);
        auto inside = Parameters::get<int>("phase field inside").value_or(0);
        auto elementVectorOut = ElementVector(*grid(),0);
        auto outside = Parameters::get<int>("phase field outside").value_or(0);
        for (auto const& e : elements(gridView())) {
            auto father = e;
            while (father.hasFather())
                father = father.father();

            if (partitions_[problem().grid()->levelGridView(0).indexSet().index(father)] == 1 && inside) {
                elementVectorIn.data()[gridView().indexSet().index(e)] = 1;
            }
            if (partitions_[problem().grid()->levelGridView(0).indexSet().index(father)] == 0 && outside) {
                elementVectorOut.data()[gridView().indexSet().index(e)] = 1;
            }
        }
        valueOf(*rhoDOF_,_mu) << valueOf(elementVectorIn) + valueOf(elementVectorOut);

        if (adaptInfo.time() < 1.e-6) {
            // initial refinement
            int ref_int = Parameters::get<int>("refinement->interface").value_or(10);
            int numCircles = Parameters::get<int>("parameters->number of circles").value_or(1);
            int initial = Parameters::get<int>("circles or initial value").value_or(0);
            if (initial) {
                double initialVal = Parameters::get<double>("phase field initial value").value_or(0);
                double randomVal = Parameters::get<double>("phase field initial value random").value_or(0);
                double radius = Parameters::get<double>("initial radius").value_or(0.15);
                double radiusIn = Parameters::get<double>("initial radius inside").value_or(0.15);
                auto randomDOF = [initialVal, randomVal, radius, radiusIn](WorldVector const &x) {
                    return (std::pow(x[0], 2) + std::pow(x[1], 2) <= std::pow(radius, 2) &&
                            std::pow(x[0], 2) + std::pow(x[1], 2) >= std::pow(radiusIn, 2)) ?
                           initialVal + randomVal * ((double) rand() / (RAND_MAX)) : 0.0;
                };
                this->getPhase() << randomDOF;
            }
            for (int i = 0; i <= ref_int; ++i) {
                if (!initial) {
                    this->getPhase() << 0.0;
                    for (int n = 0; n < numCircles; n++) {
                        //circles
                        double radius1 = Parameters::get<double>("parameters->radius1_" + std::to_string(n)).value_or(
                                0.15);
                        double radius2 = Parameters::get<double>("parameters->radius2_" + std::to_string(n)).value_or(
                                0.25);
                        WorldVector center{0.5, 0.5};
                        Parameters::get("parameters->center" + std::to_string(n), center);

                        auto circleN = [radius1, radius2, &center](WorldVector const &x) {
                            return (radius1 + radius2) * (std::sqrt(
                                    Math::sqr((x[0] - center[0]) / radius1) + Math::sqr((x[1] - center[1]) / radius2)) -
                                                          1.0);
                        };
                        this->getPhase() += clamp(phase(circleN), 0.0, 1.0); // phase field only inside the shell
                    }
                }
                this->getPhase() << valueOf(*rhoDOF_, _mu) * this->getPhase();
                problem().markElements(adaptInfo);
                problem().adaptGrid(adaptInfo);
                elementSizes.resizeZero();
                elementLevels.resizeZero();
                for (auto const &e: elements(gridView())) {
                    elementSizes.data()[gridView().indexSet().index(e)] = e.geometry().volume();
                    elementLevels.data()[gridView().indexSet().index(e)] = e.level();
                    if (e.geometry().volume() < minimumElementVolume) minimumElementVolume = e.geometry().volume();
                }
            }
            minimumElementVolume *= 2.5;
            std::cout << "minimum element volume = " << minimumElementVolume << "\n";
        } else { // if the method is called after remeshing
            this->getPhase() << valueOf(*rhoDOF_, _mu) * this->getPhase();
        }

        auto incompressibleShell = Parameters::get<int>("is shell incompressible").value_or(0);
        if (incompressibleShell) {
            // initialize concentration c with c = 1
            this->solution(_lambda2).coefficients().init(problem().solution(_lambda2).basis(), false);
            auto localView = problem().solution(_lambda2).basis().localView();
            for (const auto &el: elements(gridView())) {
                localView.bind(el);
                if (localView.size() == 0) continue;

                std::vector<double> localCoeff(localView.size(), 1.0);

                //fill rhsVector with calculated Values stored in intersectionVector
                problem().solution(_lambda2).coefficients().scatter(localView, localView.tree().child(_lambda2), localCoeff, Assigner::assign{});
            }
            this->solution(_lambda2).coefficients().finish();
        }
        valueOf(*oldSolution_,_phi) << invokeAtQP([](double phi){ return phi > 0.0; },getPhase());
        dropletVolume0_ = integrate((2.0*M_PI*X(1)*axi + (1.0 - axi)) * clamp(getPhase(),0.0,1.0) *
                invokeAtQP([](double phi) {return (phi > 0.5) ? 1 : 0;}, clamp(getPhase(),0.0,1.0)),gridView(),2);
        volume0_ = integrateShell(1.0, gridView(), partitions_, 1);
        transferInitialSolution(adaptInfo);
    }

    template <class HostGrid, class PBF>
    void NavierStokesCahnHilliard<HostGrid, PBF>::initTimestep(AdaptInfo& adaptInfo)
    {
        *oldSolution_ = *problem().solutionVector();
        valueOf(*oldVelocity_) << problem().solution(_v);
        valueOf(*oldPhi_) << problem().solution(_phi);
        valueOf(*oldPhiContinuous_) << problem().solution(_phi);
        valueOf(*oldMu_) << problem().solution(_mu);
        tau_ = adaptInfo.timestep();
        invTau_ = 1.0 / adaptInfo.timestep();

        auto line = Parameters::get<double>("line mesh").value_or(0.0);
        if (!line) {
            vx_ = integrateShell(get(getVelocity(), 0), gridView(), partitions_, 1)
                  / integrateShell(1.0, gridView(), partitions_, 1);
            vy_ = integrateShell(get(getVelocity(), 1), gridView(), partitions_, 1)
                  / integrateShell(1.0, gridView(), partitions_, 1);
            shellArea_ = integrateShell(1.0,gridView(),partitions_); // relaxation term
        } else {
            vx_ = 0.0;
            vy_ = 0.0;
            shellArea_ = 1.0; // relaxation term
        }

        auto relaxation = Parameters::get<double>("surface concentration relaxation").value_or(0.0);

        shellAreaChange_ = relaxation * (shellArea0_ - shellArea_) / shellArea0_;
        std::cout << "relative area change = " << shellAreaChange_/(relaxation + 1e-10) << "\n";

        valueOf(*oldSolution_,_phi) << invokeAtQP([](double phi){ return phi > 0.0; },getPhase());
        dropletVolume_ = integrate((2.0*M_PI*X(1)*axi + (1.0 - axi)) * clamp(getPhase(),0.0,1.0) *
                                           invokeAtQP([](double phi) {return (phi > 0.5) ? 1 : 0;}, clamp(getPhase(),0.0,1.0)),gridView(),2);
        auto delta_D = Parameters::get<double>("droplet volume conservation parameter").template value_or(0);
        auto delta_V = Parameters::get<double>("volume restore constant").template value_or(0);
        dropVolChangeOverTime_ = dropletVolume0_ > 1.e-10 ? (dropletVolume0_-dropletVolume_)/dropletVolume0_*delta_D*invTau_ : 0.0;
        if (!line) volume_ = integrateShell(1.0, gridView(), partitions_, 1);

        volChangeOverTime_ = -delta_V * (volume_-volume0_)/volume0_;
        std::cout << "volume change = " << dropletVolume_-dropletVolume0_ << ", " << dropletVolume_ << ", " << dropletVolume0_ << "\n";
    }

    template <class HostGrid, class PBF>
    void NavierStokesCahnHilliard<HostGrid, PBF>::closeTimestep(AdaptInfo& adaptInfo)
    {
        writeFiles(adaptInfo);
        auto sigma0 = Parameters::get<double>("parameters->sigma0").value_or(0);
        auto sigma1 = Parameters::get<double>("parameters->sigma1").value_or(0);
        auto B = Parameters::get<double>("bendingStiffness").value_or(0);
        DOFVector kappa(gridView(),power<2>(lagrange<1>(),flatInterleaved()),tag::no_datatransfer{});

        auto const& spb = solution(_kappaVec).basis().preBasis().subPreBasis(_5).subPreBasis();
        using SPB = decltype(spb);
        using DFL = decltype(valueOf(solution(_kappaVec)));
        using TP = decltype(_0);
        auto curvature = surfaceGridFct2D(valueOf(solution(_kappaVec)), spb,_kappaVec);

        valueOf(kappa).interpolate(problem().solution(_kappaVec),tag::assignToBulk{});

        double E_kin = integrate( (2.0*M_PI*X(1)*axi + (1.0 - axi)) * 0.5*rhoPhase()*unary_dot(getVelocity()),gridView(),4);
        double E_dissipate = integrate((2.0*M_PI*X(1)*axi + (1.0 - axi)) * -0.5*nuPhase()
                * 2.0*(2.0*get(gradientOf(problem().solution(_v,0)),0)*get(gradientOf(problem().solution(_v,0)),0)
                             + 2.0*get(gradientOf(problem().solution(_v,1)),0)*get(gradientOf(problem().solution(_v,0)),1)
                               + 2.0*get(gradientOf(problem().solution(_v,1)),1)*get(gradientOf(problem().solution(_v,1)),1)
                                 + get(gradientOf(problem().solution(_v,0)),1)*get(gradientOf(problem().solution(_v,0)),1)
                                   + get(gradientOf(problem().solution(_v,1)),0)*get(gradientOf(problem().solution(_v,1)),0))
                ,gridView(),4);
        double E_bend = integrate((2.0*M_PI*X(1)*axi + (1.0 - axi)) * 0.5*B*unary_dot(curvature),solution(_kappaVec).basis().preBasis().subPreBasis(_kappaVec).subPreBasis().surfaceGridView(),4); // integrateShell(0.5 * B * unary_dot(valueOf(kappa)),gridView(),partitions_,-1);
        double E_surfTen = integrateShell(((sigma1-sigma0) * (getOldPhase()*getOldPhase()) * (3.0 - 2.0* getOldPhase()) + sigma0),
                                          gridView(),partitions_,-1);
        double E_ch = integrate((2.0*M_PI*X(1)*axi + (1.0 - axi)) * sigma_*(eps_*0.5*unary_dot(gradientOf(getOldPhase())) + 0.25/eps_*pow<2>(getOldPhase())*pow<2>(1.0-getOldPhase()))
                ,gridView(),4);
        double E_sum_old = E_sum;
        E_sum = E_kin + E_bend + E_ch + E_surfTen;
        double dtEsum = (E_sum - E_sum_old) / adaptInfo.timestep();

        std::string path;
        Parameters::get("output directory", path);
        std::string output_filename;
        Parameters::get("output_filename", output_filename);
        output_filename = path + "/" + output_filename;
        FILE *file;
        if (adaptInfo.time() == adaptInfo.timestep()) {
            file = fopen(output_filename.c_str(), "w");
            fprintf(file, "time[s], E_kin, E_dissipate, E_bend, E_surfTen, E_ch, E_sum, dtE_sum\n");
            fclose(file);

        } else {
            file = fopen(output_filename.c_str(), "a");
            fprintf(file, "%10.6f, %10.6e, %10.6e, %10.6e, %10.6f, %10.6f, %10.6e, %10.6e\n", adaptInfo.time(), E_kin, E_dissipate,E_bend,E_surfTen,E_ch, E_sum, dtEsum);
            fclose(file);
        }

        // update element size vector
        elementSizes.resizeZero();
        elementLevels.resizeZero();
        auto maxLevel = 0.0;
        for (auto const& e : elements(gridView())) {
            elementSizes.data()[gridView().indexSet().index(e)] = e.geometry().volume();
            elementLevels.data()[gridView().indexSet().index(e)] = e.level();
            if (e.level() > maxLevel) maxLevel = e.level();
        }
        std::cout << "maximum refinement level on grid = " << maxLevel << "\n";

        auto line = Parameters::get<double>("line mesh").value_or(0.0);
        if (!line) {
            volume_ = integrateShell(1.0, gridView(), partitions_, 1);
        }
    }


template <class HostGrid, class PBF>
    void NavierStokesCahnHilliard<HostGrid, PBF>::initData(AdaptInfo& adaptInfo)
    {
        std::cout << "Navier Stokes initData() ...\n";

        partitionsVec.data() = partitions_;
        std::vector<int> facets;
        facets.resize(partitions_.size(),-1);
        auto closedShell = Parameters::get<double>("closed shell").value_or(0);
        if (!closedShell) {
            for (const auto &e: elements(problem().grid()->levelGridView(0))) {
                auto const &indexSet = problem().grid()->levelGridView(0).indexSet();
                for (const auto &is: intersections(problem().grid()->levelGridView(0), e)) {
                    if (!is.neighbor())
                        continue;

                    auto p = partitions_[indexSet.index(e)];
                    auto q = partitions_[indexSet.index(is.outside())];
                    if (p == 0 && q == 1) {
                        facets[indexSet.index(e)] = 1.0;
                        facets[indexSet.index(is.outside())] = 0.0;
                    }
                }
            }
        } else {
            facets = partitions_;
        }
        vBasis_ = std::make_shared<VBasis>(gridView(),power<2>(lagrange<2>(),flatInterleaved()));
        phiBasis_ = std::make_shared<PhiBasis>(gridView(),surface(lagrange<2>(),facets));
        phiBasisCont_ = std::make_shared<PhiBasisCont>(gridView(),lagrange<2>());

        oldVelocity_.reset(new DOFVector<VBasis>(*vBasis_));
        oldVelocity_->template setDataTransfer(tag::simple_datatransfer{});
        oldPhi_.reset(new DOFVector<PhiBasis>(*phiBasis_));
        oldPhi_->template setDataTransfer(tag::simple_datatransfer{});
        oldPhiContinuous_.reset(new DOFVector<PhiBasisCont>(*phiBasisCont_));
        oldPhiContinuous_->template setDataTransfer(tag::simple_datatransfer{});
        oldMu_.reset(new DOFVector<PhiBasis>(*phiBasis_));
        oldMu_->template setDataTransfer(tag::simple_datatransfer{});
        oldSolution_.reset(new DOFVector<Basis>(*this->globalBasis()));
        rhoDOF_.reset(new DOFVector<Basis>(*this->globalBasis()));
        nuDOF_.reset(new DOFVector<Basis>(*this->globalBasis()));


        tau_ = adaptInfo.timestep();
        invTau_ = 1.0 / adaptInfo.timestep();
        int line = Parameters::get<int>("line mesh").value_or(0);
        if (!line) {
            shellArea_ = integrateShell(1.0, gridView(), partitions_);
            shellArea0_ = integrateShell(1.0, gridView(), partitions_);
            volume_ = integrateShell(1.0, gridView(), partitions_, 1);
        } else {
            shellArea_ = 1;
            shellArea0_ = 1;
            volume_ = 1;
        }
    }
    template <class HostGrid, class PBF>
    template <class Lambda>
    void NavierStokesCahnHilliard<HostGrid, PBF>::initData(AdaptInfo& adaptInfo, Lambda const& lambda, double& vMin)
    {
        int ref_bulk = Parameters::get<int>("refinement->bulk").value_or(2);
        int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
        int doRefine  = Parameters::get<int>("refinement->refine with stretch").value_or(10);

        for (auto const& e : elements(gridView())) {
            elementSizes.data()[gridView().indexSet().index(e)] = e.geometry().volume();
            elementLevels.data()[gridView().indexSet().index(e)] = e.level();
        }
        if (doRefine) {
            auto markerVolumes = GridFunctionMarker("a-lambda", problem().grid(),
                                                    invokeAtQP([ref_int, ref_bulk, &vMin]( double lambda, double size, double level) -> int {
                                                        return size > vMin ? std::max(
                                                                std::min((int) std::trunc(lambda * 10), ref_int), 0) :
                                                               (lambda > 1.e-6 ? level : ref_bulk);
                                                    }, lambda, valueOf(elementSizes), valueOf(elementLevels)));
            problem().addMarker(Dune::wrap_or_move(std::move(markerVolumes)));
        }
        // refine by element size: if element size at the interface is larger than vMin -> refine,
        // if smaller -> do nothing,
        // else -> coarsen
        auto marker = GridFunctionMarker("b-interface", problem().grid(),
                                         invokeAtQP([ref_int, ref_bulk, &vMin](double phi, double size, int level) -> int {
                                             return phi > 0.05 && phi < 0.95 && size > vMin ? ref_int :
                                                    (phi > 0.05 && phi < 0.95 && size <= vMin ? std::min(level,ref_int) : ref_bulk);
                                         }, this->getPhase(), valueOf(elementSizes), valueOf(elementLevels)));
        problem().addMarker(Dune::wrap_or_move(std::move(marker)));
    }



    template <class HostGrid, class PBF>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillOperators(AdaptInfo& adaptInfo)
    {
        /// Navier Stokes equation
        msg("NavierStokesCahnHilliard::fillOperators()");

        auto invTau = std::ref(invTau_);
        auto mobility = std::ref(mobility_);
        auto vDiff = std::ref(dropVolChangeOverTime_);
        auto volDiff = std::ref(volChangeOverTime_);

        // <1/tau * u, v>
        auto opTime = makeOperator(tag::testvec_trialvec{}, invTau * rhoPhase());
        problem().addMatrixOperator(opTime, _v, _v);

        // <1/tau * u^old, v>
        auto opTimeOld = makeOperator(tag::testvec{}, invTau * rhoPhase() * getVelocity());
        problem().addVectorOperator(opTimeOld, _v);

        for (int i = 0; i < dow; ++i) {
            // <(u^old * nabla)u_i, v_i>
            auto opNonlin2 = makeOperator(tag::test_gradtrial{}, rhoPhase() * getVelocity());
            problem().addMatrixOperator(opNonlin2, makeTreePath(_v,i), makeTreePath(_v,i));
        }

        // sum_i <grad(u_i),grad(v_i)> + <p, div(v)> + <div(u), q>
        auto opStokes = makeOperator(tag::stokes{}, nuPhase());
        problem().addMatrixOperator(opStokes, makeTreePath(), makeTreePath());

        auto opVolCons = makeOperator(tag::test{}, volDiff);
        problem().addVectorOperator(opVolCons, makeTreePath(_p));

        // gravitational force only for phase field
        WorldVector dir{0.5, 0.5};
        Parameters::get("direction", dir);
        auto gravity = Parameters::get<double>("gravity").value_or(0.0);
        problem().addVectorOperator(zot(dir[0]*rhoPhase()* getOldPhase()*gravity), makeTreePath(_v,0));
        problem().addVectorOperator(zot(dir[1]*rhoPhase()* getOldPhase()*gravity), makeTreePath(_v,1));

        ///Cahn-Hilliard equation
        msg("CahnHilliard::fillOperators()");

        auto phi = getPhase();
        auto phiOld = getOldPhase();


        // (phi^{k+1} - phi^k)/tau
        problem().addMatrixOperator(zot(invTau), _phi, _phi);
        problem().addVectorOperator(zot(invTau * phiOld), _phi);

        // <M * grad(w_n), grad(psi')>
        problem().addMatrixOperator(sot(mobility), _phi, _mu);

        // w = ...
        problem().addMatrixOperator(zot(1.0), _mu, _mu);

        // <-a*eps * sigma * grad(phi_n), grad(psi)>
        problem().addMatrixOperator(sot(-eps_*sigma_), _mu, _phi);

        // < phi_j * d_ij f({phi^old}), psi_i >
        auto opFimpl = zot(sigma_/eps_ * (-0.5 - 3*phiOld*(phiOld - 1)));
        problem().addMatrixOperator(opFimpl, _mu, _phi);

        // < d_i f({phi^old}) - phi_i^old * d_ij f({phi^old}), psi_i >
        auto opFexpl = zot(sigma_/eps_ *  pow<2>(phiOld)*(1.5 - 2*phiOld));
        problem().addVectorOperator(opFexpl, _mu);

        // droplet volume conservation term
        auto vCons = zot(vDiff * pow((get(gradientOf(getPhase()),0)*get(gradientOf(getPhase()),0)
                                     + get(gradientOf(getPhase()),1)*get(gradientOf(getPhase()),1)),0.5),2);
        problem().addVectorOperator(vCons, _phi);



        /// coupling operators
        // <-phi^old*u^new, psi>
        auto opAdvect3 = makeOperator(tag::test_trialvec{}, gradientOf(phiOld));
        problem().addMatrixOperator(opAdvect3, _phi, _v);

        // <sigma*eps grad(phi^new)\times grad(phi^old), grad(psi)>
        auto useCapillary = Parameters::get<int>("use capillary stress").template value_or(0);
        if (useCapillary) {
            for (std::size_t i = 0; i < dow; ++i) {
                for (std::size_t j = 0; j < dow; ++j) {
                    auto force3 = makeOperator(tag::partialtest_partialtrial{i,i}, -(eps_*sigma_)*get(gradientOf(phiOld),j));
                    problem().addMatrixOperator(force3, makeTreePath(_v,j), _phi);
                }
            }
        } else {
            // <mu*grad_phi,psi>
            auto opAdvect3 = makeOperator(tag::testvec_gradtrial{}, -getW());
            problem().addMatrixOperator(opAdvect3, _v, _phi);

            // < grad(phi) * phi_j * d_ij f({phi^old}), psi_i >
            auto opFimplS = makeOperator(tag::testvec_trial{}, 2.0 * sigma_/eps_ * gradientOf(phiOld) * (0.5 + 3*phiOld*(phiOld - 1)));
            problem().addMatrixOperator(opFimplS, _v, _phi);

            // < grad(phi) * d_i f({phi^old}) - phi_i^old * d_ij f({phi^old}), psi_i >
            auto opFexplS = makeOperator(tag::testvec_gradtrial{}, 2.0 * sigma_/eps_ * pow<2>(phiOld)*(1.5 - 2*phiOld));
            problem().addMatrixOperator(opFexplS, _v, _phi);

            
        }


        // axisymmetric terms
        if (axi) {
            // Navier Stokes
            auto laplaceVAxi00 = makeOperator(tag::test_partialtrial{1}, -nuPhase()/X(1),4);
            problem().addMatrixOperator(laplaceVAxi00,makeTreePath(_v,0), makeTreePath(_v,0));

            auto laplaceVAxi01 = makeOperator(tag::test_partialtrial{0}, -nuPhase()/X(1),4);
            problem().addMatrixOperator(laplaceVAxi01,makeTreePath(_v,0), makeTreePath(_v,1));

            auto laplaceVAxi11 = makeOperator(tag::test_partialtrial{1}, -2.0 * nuPhase()/X(1),4);
            problem().addMatrixOperator(laplaceVAxi11,makeTreePath(_v,1), makeTreePath(_v,1));


            auto laplaceVAxi2 = makeOperator(tag::test_trial{}, 2.0 * nuPhase()/ (X(1) * X(1)), 4);
            problem().addMatrixOperator(laplaceVAxi2,makeTreePath(_v,1), makeTreePath(_v,1));

            auto divUAxi = makeOperator(tag::test_trial{}, 1.0/X(1), 4);
            problem().addMatrixOperator(divUAxi, _p, makeTreePath(_v,1));

            // Cahn-Hilliard
            auto laplaceMuAxi = makeOperator(tag::test_partialtrial{1},-mobility/X(1),4);
            problem().addMatrixOperator(laplaceMuAxi,_phi,_mu);

            auto laplacePhiAxi = makeOperator(tag::test_partialtrial{1},(eps_*sigma_)/X(1),4);
            problem().addMatrixOperator(laplacePhiAxi,_mu,_phi);

            // Coupling term
            if (useCapillary) {
                auto opSurfAxi = makeOperator(tag::test_partialtrial{0},
                                              (eps_*sigma_)/X(1) * partialDerivativeOf(getOldPhase(),1),4);
                auto opSurfAxi2 = makeOperator(tag::test_partialtrial{1},
                                               (eps_*sigma_)/X(1) * partialDerivativeOf(getOldPhase(),1),4);
                problem().addMatrixOperator(opSurfAxi, makeTreePath(_v,0),_phi);
                problem().addMatrixOperator(opSurfAxi2, makeTreePath(_v,1),_phi);
            }
        }
    }

    template <class HostGrid, class PBF>
    template<class Container, class LambdaResult>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillCouplingOperatorALE(Container const& laplaceSolution, LambdaResult&& v_cell) {
        for (unsigned i = 0; i < dow; i++) {
            auto opConvectGrid = makeOperator(tag::test_gradtrial{}, rhoPhase() * (v_cell - laplaceSolution), 2);
            problem().addMatrixOperator(opConvectGrid, makeTreePath(_v, i), makeTreePath(_v, i));
        }
        auto opConvectGridPhi = makeOperator(tag::test_gradtrial{}, v_cell - laplaceSolution, 2);
        problem().addMatrixOperator(opConvectGridPhi, _phi, _phi);
    }
    
    template <class HostGrid, class PBF>
    template <class DGVF, class KV, class EV>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillSurfaceOperators(DGVF const& deltaP,
                                                                       DGVF const& deltaPhi,
                                                                       KV const& kappaVecOld,
                                                                       EV const& normalVec)
    {
        auto tau = std::ref(tau_);
        auto invTau = std::ref(invTau_);
        auto perm = std::ref(permeability_);
        auto sAC = std::ref(shellAreaChange_);
        using GV = decltype(problem().grid()->leafGridView());

        auto const& partitionsLeaf = problem().solution(_xS).basis().preBasis().subPreBasis(_xS).subPreBasis().partitionsLeaf();
        auto const& facetsVec = problem().solution(_xS).basis().preBasis().subPreBasis(_xS).subPreBasis().facets();
        auto const& is = gridView().indexSet();
        using IS = decltype(is);


        // xS =
        auto opCoords = makeOperator(tag::surface_testvec_trialvec{},1.0, 2);
        problem_.addMatrixOperator(opCoords,_xS,_xS);

        auto normalMovement = Parameters::get<int>("move grid points with normal velocity").value_or(0);
        if (normalMovement) {
            // -\tau*(velocity*n)*n
            for (int i = 0; i < dow; i++) {
                auto opVel = makeOperator(tag::surface_testvec_trial_special<IS>{i}, tau * -1.0,2);
                problem_.addMatrixOperator(opVel, _xS, makeTreePath(_vS,i));
            }
        } else {
            // -\tau*velocity
            auto opVel = makeOperator(tag::surface_testvec_trialvec{}, tau * -1.0, 2);
            problem_.addMatrixOperator(opVel,_xS,_vS);
        }

        // permeability operators
        auto use = Parameters::get<int>("use p0 for permeability").value_or(0);
        auto sigma = Parameters::get<int>("parameters->sigma0").value_or(0);
        auto Kb = Parameters::get<double>("bendingStiffness").value_or(0.0);
        volume0_ = integrateShell(1.0, gridView(), partitions_, 1);

        double rCirc = std::sqrt(volume0_/M_PI);
        auto dow = GV::dimension;
        double p0 = 0.0;
        if (use) {
            if (dow == 2 && !axi)
                p0 = sigma / rCirc - 0.5 * Kb / std::pow(rCirc, 3);
            else if (axi || dow == 3)
                p0 = 2.0 * sigma / rCirc;
        }
        // +\tau*permeability*pDiff*1-abs(\phiDiff)*normal
        for (int i = 0; i < dow; i++) {
            auto opPermIn = makeOperator(tag::surface_test_trial<IS>{0}, tau * -perm * abs(deltaPhi) * valueOf(normalVec[i]), 2);
            problem_.addMatrixOperator(opPermIn, makeTreePath(_xS,i),_p);
            auto opPermOut = makeOperator(tag::surface_test_trial<IS>{1}, tau * -perm * abs(deltaPhi) * valueOf(normalVec[i]), 2);
            problem_.addMatrixOperator(opPermOut, makeTreePath(_xS,i),_p);
            if (use) {
                auto opPerm = makeOperator(tag::surface_test{}, tau * -perm * p0 * abs(deltaPhi) * valueOf(normalVec[i]), 2);
                problem_.addVectorOperator(opPerm, makeTreePath(_xS,i));
            }
        }

        // = coords
        auto opX = makeOperator(tag::surface_testvec{}, X(), 2);
        problem_.addVectorOperator(opX,_xS);

        // L =
        auto opL = makeOperator(tag::surface_testvec_trialvec{},(axi*X(1) + 1.0-axi), 2);
        problem_.addMatrixOperator(opL,_kappaVec,_kappaVec);

        // = -k0*n
        auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0);
        if (k0) {
            for (int i = 0; i < dow; i++) {
                auto opSpontaneous1 = makeOperator(tag::surface_test{},
                                                   -k0 * valueOf(normalVec[i]) * (axi * X(1) + 1.0 - axi), 2);
                problem_.addVectorOperator(opSpontaneous1, makeTreePath(_kappaVec, i));
            }
        }

        if (axi) {
            // <grad x, grad v>
            for (int i = 0; i < dow; i++) {
                auto opLB = makeOperator(tag::surface_gradtest_gradtrial_axi{}, X(1), 2);
                problem_.addMatrixOperator(opLB, makeTreePath(_kappaVec, i), makeTreePath(_xS, i));
            }
        } else {
            // <grad x, grad v>
            for (int i = 0; i < dow; i++) {
                auto opLB = makeOperator(tag::surface_gradtest_gradtrial{}, 1.0, 2);
                problem_.addMatrixOperator(opLB, makeTreePath(_kappaVec, i), makeTreePath(_xS, i));
            }
        }

        // axisymmetric terms for _kappaVec
        if (axi) {
            auto opAxi4 = makeOperator(tag::surface_test_trial{},1.0/X(1),4);
            problem_.addMatrixOperator(opAxi4,makeTreePath(_kappaVec,1),makeTreePath(_xS,1));
        }

        auto incompressibleShell = Parameters::get<int>("is shell incompressible").value_or(0);
        if (!incompressibleShell) {
            // d_t lambda1 =
            auto opTimeL1 = makeOperator(tag::surface_test_trial{}, 1.0, 2);
            problem_.addMatrixOperator(opTimeL1, _lambda1, _lambda1);
        } else {
            // div v_S =
            auto opDivVS = makeOperator(tag::surface_test_divtrialvec{}, 1.0, 2);
            problem_.addMatrixOperator(opDivVS, _lambda1, _vS);

            auto theta1 = Parameters::get<double>("surface inextensibility diffusion").value_or(0.0);
            // = \Delta_\Gamma lambda1
            auto opLaplL1 = makeOperator(tag::surface_gradtest_gradtrial{}, theta1, 2);
            problem_.addMatrixOperator(opLaplL1, _lambda1, _lambda1);

            // lagrange multiplier in NS eq
            auto opSurfGradSigma = makeOperator(tag::surface_divtestvec_trial{}, (X(1)*axi + (1.0-axi)*1.0),2);
            problem_.addMatrixOperator(opSurfGradSigma, _fShell, _lambda1);

            if (axi) {
                //  v_s_r / R =
                auto opDivVSAxi = makeOperator(tag::surface_test_trial{}, 1.0/X(1), 4);
                problem_.addMatrixOperator(opDivVSAxi, _lambda1, makeTreePath(_vS,1));

                auto laplL1Axi = makeOperator(tag::surface_test_partialtrial<IS>{1},theta1/X(1),4);
                problem().addMatrixOperator(laplL1Axi,_lambda1,_lambda1);

                auto surfGradSigmaAxi = makeOperator(tag::surface_test_trial{},1.0,4);
                problem().addMatrixOperator(surfGradSigmaAxi,makeTreePath(_fShell,1),_lambda1);

            }

            // div v_S = relax*(c-1)/c
            auto relax = makeOperator(tag::surface_test{}, sAC, 2);
            problem_.addVectorOperator(relax, _lambda1);
        }

        // lambda2 = 0
        auto opTimeL2 = makeOperator(tag::surface_test_trial{}, 1.0,2);
        problem_.addMatrixOperator(opTimeL2,_lambda2,_lambda2);

        // F = ... for force solution
        auto op = makeOperator(tag::surface_testvec_trialvec{},(X(1)*axi + (1.0-axi)*1.0), 2);
        problem_.addMatrixOperator(op,_fShell,_fShell);

        // v = shell force on Gamma
        auto opForce = makeOperator(tag::surface_testvec_trialvec{},-1.0, 2);
        problem_.addMatrixOperator(opForce,_v,_fShell);

        // vS =
        auto opVs = makeOperator(tag::surface_testvec_trialvec{},1.0,2);
        problem_.addMatrixOperator(opVs,_vS,_vS);

        // vS =
        auto opV = makeOperator(tag::surface_testvec_trialvec{},-1.0,2);
        problem_.addMatrixOperator(opV,_vS,_v);

        // phiS =
        auto opPhis = makeOperator(tag::surface_test_trial{},1.0,2);
        problem_.addMatrixOperator(opPhis,_phiS,_phiS);

        // phiS =
        auto opPhi = makeOperator(tag::surface_test_trial{},-1.0,2); //only outside, if phasefield inside, add another phiS!
        problem_.addMatrixOperator(opPhi,_phiS,_phi);


        /// contact angle condition
        auto inside = Parameters::get<int>("phase field inside").value_or(0);
        auto outside = Parameters::get<int>("phase field outside").value_or(0);
        auto sigma0 = Parameters::get<double>("parameters->sigma0").value_or(0);
        auto sigma1 = Parameters::get<double>("parameters->sigma1").value_or(0);
        if (inside && outside) { //if both operators from in and out are added, sigmas have to be halved
            sigma0 *= 0.5;
            sigma1 *= 0.5;
        }

        if (outside) {
            auto angle = makeOperator(tag::surface_test_trial<IS>{0, is, partitionsLeaf, facetsVec},
                                      -6.0 * (1.0 - getOldPhase()) * (sigma1 - sigma0), 4);
            problem_.addMatrixOperator(angle, _mu,_phi);

            if (!incompressibleShell) {
                auto surfTenPhase = makeOperator(tag::surface_testvec_trialvec{},
                                                 -((sigma1 - sigma0) * (getOldPhase() * getOldPhase()) *
                                                   (3.0 - 2.0 * getOldPhase()) + sigma0) * (X(1)*axi + (1.0-axi)*1.0),
                                                 4); //argument is scalar multiplied with unit outer normal
                problem_.addMatrixOperator(surfTenPhase, _fShell, _kappaVec);

                auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0);
                if (k0) {
                    //add spontaneous curvature part to the surface tension force
                    for (size_t i = 0; i < dow; ++i) {
                        auto surfTenPhaseK0 = makeOperator(tag::surface_test_trial{},
                                                           -k0 * valueOf(normalVec[i]) *
                                                           ((sigma1 - sigma0) * getOldPhase() *
                                                            (3.0 - 2.0 * getOldPhase()) + sigma0) *
                                                           (X(1) * axi + (1.0 - axi) * 1.0),
                                                           4);
                        problem_.addMatrixOperator(surfTenPhaseK0, makeTreePath(_fShell,i), _phi);
                    }
                }
            }
            auto sign3 = Parameters::get<double>("parameters->couplingSign3").value_or(0);
            auto bound = makeOperator(tag::surface_testvec{},
                                      sign3 * 6.0 * (getOldPhase()) * (1.0 - getOldPhase()) * (sigma1 - sigma0) *
                                      gradientOf(getOldPhase()) * (X(1)*axi + (1.0-axi)*1.0), 4);
            problem_.addVectorOperator(bound, _fShell);

            if (!incompressibleShell) {
                auto boundSGrad = makeOperator(tag::surface_testvec_gradtrial{},
                                               -6.0 * (getOldPhase()) * (1.0 - getOldPhase()) * (sigma1 - sigma0)
                                               * (X(1)*axi + (1.0-axi)*1.0),
                                               4);
                problem_.addMatrixOperator(boundSGrad, _fShell, _phiS);
            }
        }

        if (inside) {
            auto angle = makeOperator(tag::surface_test_trial<IS>{1, is, partitionsLeaf, facetsVec},
                                      -6.0 * (1.0 - getOldPhase()) * (sigma1 - sigma0), 4);
            problem_.addMatrixOperator(angle, _mu,_phi);

            if (!incompressibleShell) {
                auto surfTenPhase = makeOperator(tag::surface_testvec_trialvec<IS>{1},
                                                 -((sigma1 - sigma0) * (getOldPhase() * getOldPhase()) *
                                                   (3.0 - 2.0 * getOldPhase()) + sigma0) * (X(1)*axi + (1.0-axi)*1.0),
                                                 4); //argument is scalar multiplied with unit outer normal
                problem_.addMatrixOperator(surfTenPhase, _fShell, _kappaVec);

                auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0);
                if (k0) {
                    //add spontaneous curvature part to the surface tension force
                    for (size_t i = 0; i < dow; ++i) {
                        auto surfTenPhaseK0 = makeOperator(tag::surface_test_trial<IS>{1},
                                                           -k0 * valueOf(normalVec[i]) *
                                                           ((sigma1 - sigma0) * getOldPhase() *
                                                            (3.0 - 2.0 * getOldPhase()) + sigma0) *
                                                           (X(1) * axi + (1.0 - axi) * 1.0),
                                                           4);
                        problem_.addMatrixOperator(surfTenPhaseK0, makeTreePath(_fShell,i), _phi);
                    }
                }
            }
            auto sign3 = Parameters::get<double>("parameters->couplingSign3").value_or(0);
            auto bound = makeOperator(tag::surface_testvec<IS>{1},
                                      sign3 * 6.0 * (getOldPhase()) * (1.0 - getOldPhase()) * (sigma1 - sigma0) *
                                      gradientOf(getOldPhase())* (X(1)*axi + (1.0-axi)*1.0), 4);
            problem_.addVectorOperator(bound, _fShell);

            if (!incompressibleShell) {
                auto boundSGrad = makeOperator(tag::surface_testvec_gradtrial<IS>{1},
                                               -6.0 * (getOldPhase()) * (1.0 - getOldPhase()) * (sigma1 - sigma0)
                                               * (X(1)*axi + (1.0-axi)*1.0),
                                               4);
                problem_.addMatrixOperator(boundSGrad, _fShell, _phi);
            }
        }

        // line tension
        auto lineTension = Parameters::get<double>("lineTension").value_or(0);
        if (axi) {
            // -lineTension*1/R*|\nabla(\phi)*(0,1)^T*X(1)
            auto opLT = makeOperator(tag::surface_test{},
                                     -lineTension * pow(pow<2>((1.0 - pow<2>(valueOf(normalVec[0]))) * get(gradientOf(getPhase()),0)
                                                                - valueOf(normalVec[0])*valueOf(normalVec[1]) * get(gradientOf(getPhase()),1))
                                                      + pow<2>((1.0 - pow<2>(valueOf(normalVec[1]))) * get(gradientOf(getPhase()),1)
                                                                - valueOf(normalVec[1])*valueOf(normalVec[0]) * get(gradientOf(getPhase()),0)),0.5), 2); //two_norm(gradientOf(getPhase())), 2);
            problem_.addVectorOperator(opLT, makeTreePath(_fShell, 1));
        }

    }

    /// compute the surface tension force either explicitly with finite differences or implicitly with
    /// surface finite elements
    template <class HostGrid, class PBF>
    template <class STF, class EV>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillSurfaceTensionForceOperators(STF& stForce,
                                                                                   EV const& normalVec)
    {
        using GV = decltype(grid()->levelGridView(0));
        GridView const& gv = gridView();
        auto st = Parameters::get<double>("surfaceTension").value_or(0.0);

        auto useExplicitST = Parameters::get<int>("explicit surface tension force").value_or(0);
        if (useExplicitST) {
            auto opShellST = makeOperator(tag::surface_testvec{},
                                          stForce.getForce(gv), 1.0);
            problem_.addVectorOperator(opShellST, _fShell);
        } else {
            auto opST = makeOperator(tag::surface_testvec_trialvec{},-st * (X(1)*axi + (1.0-axi)*1.0), 2);
            problem_.addMatrixOperator(opST,_fShell,_kappaVec);

            auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0);
            if (k0) {
                //add spontaneous curvature part to the surface tension force
                for (size_t i = 0; i < dow; ++i) {
                    auto surfTenPhaseK0 = makeOperator(tag::surface_test{},
                                                       st * k0 * valueOf(normalVec[i]) *
                                                       (X(1) * axi + (1.0 - axi) * 1.0),
                                                       4);
                    problem_.addVectorOperator(surfTenPhaseK0, makeTreePath(_fShell,i));
                }
            }
        }
    }

    /// compute the stretching tension force either explicitly with finite differences or implicitly with
    /// surface finite elements
    template <class HostGrid, class PBF>
    template <class SF, class EV, class DV, class KV, class EVV>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillStretchingForceOperators(SF& stretchingForce,
                                                                               EV const& distances,
                                                                               DV const& lambda2,
                                                                               KV const& kappaVecOld,
                                                                               EVV const& normalVec)
    {
        using GV = decltype(grid()->levelGridView(0));
        GridView const& gv = gridView();
        auto Ka = Parameters::get<double>("areaDilation").value_or(0.0);
        auto Ks = Parameters::get<double>("areaShear").value_or(0.0);
        auto tau = std::ref(tau_);

        auto useExplicitS = Parameters::get<int>("explicit stretching force").value_or(0);
        if (useExplicitS) {
           auto opShell = makeOperator(tag::surface_testvec{},
                                        stretchingForce.getForce(gv), 1.0);
           problem_.addVectorOperator(opShell, _fShell);
        } else {
            // lambda1^n+1 + \tau v^n+1\grad lambda1^n = lambda1^n + \tau (lambda1^n div v^n+1)
            // \frac{\partial\lambda_1}{\partial t} + \mathbf{v}\cdot\nabla\lambda_1 - \lambda_1\nabla_{\Gamma}\cdot\mathbf{v}=0
            auto incompressibleShell = Parameters::get<int>("is shell incompressible").value_or(0);

            if (!incompressibleShell) {
                // = lambda1Old
                auto opX = makeOperator(tag::surface_test{}, valueOf(distances) - 1.0, 2);
                problem_.addVectorOperator(opX, _lambda1);

                // -tau <lambda1 div v , psi>
                auto opL1DivV = makeOperator(tag::surface_test_divtrialvec{},
                                             tau * -valueOf(distances), 2);
                problem_.addMatrixOperator(opL1DivV, _lambda1, _vS);

                if (axi && !Ks) {
                    auto opAxiL2 = makeOperator(tag::surface_test_trial{},
                                                tau * -valueOf(distances) / X(1), 2);
                    problem_.addMatrixOperator(opAxiL2, _lambda1, makeTreePath(_vS, 1));
                }

                // stretching force operators according to
                // F_stretch = (kappa*n -grad) ( (Ka + Ks) lambda1 + (Ka-Ks) lambda2) - 2Ks / X(1) (lambda1 - lambda2) (0,1)^T
                // if Ks != 0, but if Ks == 0, then
                // F_stretch == Ka*(kappa*n -grad) (lambda1*lambda2), where distances = lambda1*lambda2 (computed in helperFunctions.hpp)
                auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0.0);
                for (size_t i = 0; i < dow; ++i) {
                    auto opS1 = makeOperator(tag::surface_test_trial{},
                                             -(Ka + Ks) * (X(1) * axi + (1.0 - axi) * 1.0) *
                                             (valueOf(kappaVecOld,i) + k0 * valueOf(normalVec[i])),
                                             2); // remember: _kappaVec = -kappa*n...
                    problem_.addMatrixOperator(opS1, makeTreePath(_fShell,i), _lambda1);
                }
                auto opS2 = makeOperator(tag::surface_testvec_gradtrial{},
                                         -(Ka + Ks) * (X(1)*axi + (1.0-axi)*1.0), 2);
                problem_.addMatrixOperator(opS2, _fShell, _lambda1);

                if (axi && Ks) {
                    // = lambda2Old
                    auto opX = makeOperator(tag::surface_test{}, valueOf(lambda2) - 1.0 , 2);
                    problem_.addVectorOperator(opX, _lambda2);

                    auto opAxiL2 = makeOperator(tag::surface_test_trial{},
                                                tau * -valueOf(lambda2) / X(1), 2);

                    problem_.addMatrixOperator(opAxiL2, _lambda2, makeTreePath(_vS, 1));

                    for (size_t i = 0; i < dow; ++i) {
                        auto opS3 = makeOperator(tag::surface_test_trial{},
                                                 -(Ka - Ks) * X(1) * (valueOf(kappaVecOld, i) + k0 * valueOf(normalVec[i])), 2);
                        problem_.addMatrixOperator(opS3, makeTreePath(_fShell, i), _lambda2);
                    }

                    auto opS4 = makeOperator(tag::surface_testvec_gradtrial{},
                                             -(Ka - Ks) * X(1), 2);
                    problem_.addMatrixOperator(opS4, _fShell, _lambda2);

                    auto opS5 = makeOperator(tag::surface_test_trial{},
                                             -2.0 * Ks, 2);
                    problem_.addMatrixOperator(opS5, makeTreePath(_fShell, 1), _lambda1);

                    auto opS6 = makeOperator(tag::surface_test_trial{},
                                             2.0 * Ks, 2);
                    problem_.addMatrixOperator(opS6, makeTreePath(_fShell, 1), _lambda2);
                }
            }
        }

    }

    /// compute the bending force either explicitly with finite differences or implicitly with
    /// surface finite elements
    template <class HostGrid, class PBF>
    template <class BF, class KV, class GF, class EV>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillBendingForceOperators(BF& bendingForce,
                                                                            KV const& kappaVecOld,
                                                                            GF const& gaussian,
                                                                            EV const& normalVec)
    {
        using GV = decltype(grid()->levelGridView(0));
        GridView const& gv = gridView();
        auto B = Parameters::get<double>("bendingStiffness").value_or(0.0);
        auto k0 = Parameters::get<double>("spontaneous curvature").value_or(0.0);
        auto useExplicitB = Parameters::get<int>("explicit bending force").value_or(0);

        if (useExplicitB) {
            auto opShell = makeOperator(tag::surface_testvec{},
                                        bendingForce.getForce(gv), 1.0);
            problem_.addVectorOperator(opShell, _fShell);
        } else {
            // <|kappaVec|^2, div v>
            auto const& is = gridView().indexSet();
            using IS = decltype(is);

            auto nonlin1 = makeOperator(tag::surface_divtestvec_trial{},
                                        -0.5 * (X(1)*axi + (1.0-axi)*1.0) * B * valueOf(kappaVecOld,0),2);
            problem_.addMatrixOperator(nonlin1, _fShell, makeTreePath(_kappaVec,0));

            auto nonlin1b = makeOperator(tag::surface_divtestvec_trial{},
                                        -0.5 * (X(1)*axi + (1.0-axi)*1.0) * B * valueOf(kappaVecOld,1),2);
            problem_.addMatrixOperator(nonlin1b, _fShell, makeTreePath(_kappaVec,1));


            // <grad kappaVec, grad v>
            for (std::size_t i = 0; i < dow; ++i) {
                auto laplaceKappa = makeOperator(tag::surface_gradtest_gradtrial{}, -B * (X(1)*axi + (1.0-axi)*1.0) ,2);
                problem_.addMatrixOperator(laplaceKappa, makeTreePath(_fShell,i), makeTreePath(_kappaVec,i));
            }

            if (dow == 3 || axi) {
                // < div kappaVec, div v>
                auto divKappa = makeOperator(tag::surface_divtestvec_divtrialvec{}, -B * (X(1)*axi + (1.0-axi)*1.0) , 2);
                problem_.addMatrixOperator(divKappa, _fShell, _kappaVec);

                // <grad kappaVec, P((grad v)^T + grad v) P>
                auto PGradP = makeOperator(tag::rateOfDeformation{}, B * (X(1)*axi + (1.0-axi)*1.0) , 2);
                problem_.addMatrixOperator(PGradP, _fShell, _kappaVec);
            } else {
                // < div kappaVec, div v>
                auto divKappa = makeOperator(tag::surface_divtestvec_divtrialvec{}, B, 2);
                problem_.addMatrixOperator(divKappa, _fShell, _kappaVec);
            }
            if (axi) {
                // < 1/(2)R |\kappaVec|, v_r>
                for (std::size_t i = 0; i < dow; ++i) {
                    auto axiKappa1 = makeOperator(tag::surface_test_trial{},
                                                  -0.5*B* valueOf(kappaVecOld,i), 2);
                   problem_.addMatrixOperator(axiKappa1, makeTreePath(_fShell,1), makeTreePath(_kappaVec,i));
                }

                auto axi3 = makeOperator(tag::surface_divtestvec_trial{},
                                         -B,2);
                problem_.addMatrixOperator(axi3, _fShell, makeTreePath(_kappaVec,1));

                auto axi4 = makeOperator(tag::surface_test_divtrialvec{},
                                         -B,2);
                problem_.addMatrixOperator(axi4, makeTreePath(_fShell,1), _kappaVec);

            }
            if (k0) {
                /*
                auto spontaneous1 = makeOperator(tag::surface_testvec_normal{},
                                                 B * 2.0 * k0 * makeGridFunction(gaussian,gridView()) * (X(1)*axi + (1.0-axi)*1.0) ,2);
                problem_.addVectorOperator(spontaneous1, _fShell);

                auto spontaneous2 = makeOperator(tag::surface_testvec_trialvec{},
                                                 B * 0.5 * k0 * k0 * (X(1)*axi + (1.0-axi)*1.0) ,2);
                problem_.addMatrixOperator(spontaneous2, _fShell, _kappaVec);
                */
                for (std::size_t i = 0; i < dow; ++i) {
                    auto spontaneousBGN1 = makeOperator(tag::surface_divtestvec_trial{},
                                                        -B * k0 * valueOf(normalVec[i]) * (X(1)*axi + (1.0-axi)*1.0),4);
                    problem_.addMatrixOperator(spontaneousBGN1, _fShell, makeTreePath(_kappaVec,i));

                    for (std::size_t j = 0; j < dow; ++j) {
                        auto spontaneousBGN2 = makeOperator(tag::surface_partialtest_trial<IS>{j},
                                                            B * k0 * valueOf(normalVec[i]) * (X(1)*axi + (1.0-axi)*1.0), 2);
                        problem_.addMatrixOperator(spontaneousBGN2, makeTreePath(_fShell, i), makeTreePath(_kappaVec, j));

                        auto spontaneousBGN3 = makeOperator(tag::surface_partialtest<IS>{j},
                                                            -B * k0 * k0 * valueOf(normalVec[i]) * valueOf(normalVec[j]) * (X(1)*axi + (1.0-axi)*1.0), 2);
                        problem_.addVectorOperator(spontaneousBGN3, makeTreePath(_fShell, i));

                    }
                }
            }
        }
    }
    

    template <class HostGrid, class PBF>
    void NavierStokesCahnHilliard<HostGrid, PBF>::fillBoundaryConditions(AdaptInfo& adaptInfo)
    {
        msg("NavierStokesCahnHilliard::fillBoundaryConditions()");

        //find corner coordinates of the grid
        double xmin=100000,ymin=100000,xmax=-100000,ymax=-100000;
        for (auto const& el : elements(gridView())) {
            for (auto const& is : intersections(gridView(),el)) {
                if (is.neighbor()) //only consider boundary intersections
                    continue;
                for (int i = 0; i<is.geometry().corners(); i++) {
                    xmin = std::min(xmin,is.geometry().corner(i)[0]);
                    ymin = std::min(ymin,is.geometry().corner(i)[1]);
                    xmax = std::max(xmax,is.geometry().corner(i)[0]);
                    ymax = std::max(ymax,is.geometry().corner(i)[1]);
                }
            }
        }
        FieldVector<double,2> zero(0);

        auto bottom = [ymin](auto const& x){ return x[1] <= ymin + 1e-7; }; // define boundary
        auto left = [xmin](auto const& x){ return x[0] <= xmin + 1e-7; }; // define boundary
        auto right = [xmax](auto const& x){ return x[0] >= xmax - 1e-7; }; // define boundary
        auto top = [ymax](auto const& x){ return x[1] >= ymax - 1e-7; }; // define boundary


        // zero velocity on top boundary
        //problem().addDirichletBC(3, _v, _v, zero);

        // no-slip velocity
        int line = Parameters::get<int>("line mesh").value_or(0);
        if (axi || line) {
            problem().addDirichletBC(bottom, makeTreePath(_v,1), makeTreePath(_v,1), 0.0);
        } else {
            problem().addDirichletBC(bottom, _v, _v, zero);
        }

        if(line) {
            problem().addDirichletBC(top, makeTreePath(_v,1), makeTreePath(_v,1), 0.0);
        } else {
            problem().addDirichletBC(top, _v, _v, zero);
        }

        problem().addDirichletBC(left, _v, _v, zero);
        problem().addDirichletBC(right, _v, _v, zero);


        // prescribe pressure
        auto predicate = [](auto const& x){ return x[0] > 87 && x[1] > 62.9; }; // define boundary
        problem().addDirichletBC(predicate, _p, _p, 0.0);
        problem().addDirichletBC(left, _p, _p, 0.0);
        problem().addDirichletBC(right, _p, _p, 0.0);

        //no curvature and shell force in y direction on symmetry axis
        int doConstr = 0;
        Parameters::get("axisymmetric dirichlet", doConstr);
        if ((axi || line) && doConstr) {
            auto zeroFct = [](auto const& x){ return 0.0; }; // define boundary
            problem().addConstraint(
                    AxisymmetricDirichletBC{*globalBasis(), makeTreePath(_fShell,1),makeTreePath(_fShell,1), zeroFct});
            problem().addConstraint(
                    AxisymmetricDirichletBC{*globalBasis(), makeTreePath(_kappaVec,1),makeTreePath(_kappaVec,1), zeroFct});
            problem().addConstraint(
                    AxisymmetricDirichletBC{*globalBasis(), makeTreePath(_vS,1),makeTreePath(_vS,1), zeroFct});
    //        problem().addConstraint(
    //                AxisymmetricDirichletBC{*globalBasis(), _lambda1,_lambda1, zeroFct});
    //        problem().addConstraint(
    //                AxisymmetricDirichletBC{*globalBasis(), _lambda2,_lambda2, zeroFct});
        }

    }

    template <class HostGrid, class PBF>
    template <class Expr, class GV>
    auto NavierStokesCahnHilliard<HostGrid, PBF>::integrateShell(Expr&& expr, GV const& gridView, std::vector<int> const& partitions, int part)
    {
        auto axi = Parameters::get<int>("axisymmetric").value_or(0);
        auto&& gridFct = makeGridFunction(FWD(expr), gridView);
        auto&& y = makeGridFunction(X(1), gridView);
        auto localFct = localFunction(FWD(gridFct));
        auto localY = localFunction(FWD(y));
        using GridFct = TYPEOF(gridFct);
        using Range = typename GridFct::Range;
        Range result(0);

        auto const& lvgv = gridView.grid().levelGridView(0);
        for (auto const& element : elements(gridView, Dune::Partitions::interior)) {
            auto fatherEl = element;
            while (fatherEl.hasFather())
                fatherEl = fatherEl.father();

            //if part >= 0, integrate over subdomain with number "part", else integrate over \Gamma
            if (part >= 0) {
                // integrate only if element index is different from zero
                if (part != partitions[lvgv.indexSet().index(fatherEl)])
                    continue;

                auto geometry = element.geometry();

                localFct.bind(element);
                localY.bind(element);
                auto const &quad = Dune::QuadratureRules<typename GV::ctype, GV::dimension>::rule(element.type(), 42);

                for (auto const qp: quad)
                    result += (axi*2.0*M_PI*localY(qp.position()) + (1.0-axi))*localFct(qp.position()) * geometry.integrationElement(qp.position()) * qp.weight();
                localFct.unbind();
            } else {
                for (auto const& is : intersections(gridView,element)) {
                    if (!is.neighbor())
                        continue;

                    auto fatherOutside = is.outside();
                    while (fatherOutside.hasFather())
                        fatherOutside = fatherOutside.father();

                    auto p = partitions[lvgv.indexSet().index(fatherEl)];
                    auto q = partitions[lvgv.indexSet().index(fatherOutside)];

                    if (p == 0 && q == 1) {
                        auto geometry = is.geometry();

                        localFct.bind(element);
                        auto const &quad = Dune::QuadratureRules<typename GV::ctype, GV::dimension - 1>::rule(is.type(), 42);

                        for (auto const qp: quad)
                            result += (axi*2.0*M_PI*geometry.global(qp.position())[1] + (1.0-axi)) * localFct(geometry.global(qp.position())) * geometry.integrationElement(qp.position()) * qp.weight();

                        localFct.unbind();
                    }

                }
            }
        }

        return gridView.comm().sum(result);
    }


} // end namespace AMDiS
